<!DOCTYPE html>
<html>
<head>
<title>Yearbook Business System - Database Setup</title>
</head>
<body>
<h1>Yearbook Business System Installer - Database Creation - Step 1</h1>

<?php

/*Installer - Page 1
Written by Jeffrey King
Language: PHP
*/

//Check if config file is writable.  If not, produce an error
if(!is_writable('../inc/config.php'))
{
	echo "The 'config.php' file is not writable.<br>Please execute 'chmod 777 inc/config.php' in the root directory of this repo to allow the script to write to this file.";
	exit;
}

//Check if uploads directory is writable.  If not, produce an error
if(!is_writable('../uploads'))
{
	echo "The 'uploads' directory is not writable.<br>Please execute 'chmod 777 uploads' in the root directory of this repo to allow the script to write to this directory.";
	exit;
}


?>
<p>Please enter your database information below so that we may set up your tables.</p>

<form action="install2.php" method="post">
<br>Database Host: <input type="text" name="dbhost" required>
<br>Database Username: <input type="text" name="dbuser" required>
<br>Database User Password: <input type="password" name="dbpass" required>
<br>Database Name: <input type="text" name="dbname" required>
<br><input type="submit" value="Create Tables">
</form>
<br><br>
<p>Please note - by clicking submit, any previous table information and previous configuration files will be overwritten.</p>

</body></html>
