<html>
<head>
<title>Yearbook Business System - Database Setup</title>
</head>
<body>
<?php
/* Installer - Script 2
Written by: Jeffrey King
Language: PHP, JavaScript

This script serves 2 purposes:
- To create the configuration file and write it, along with create the databases and popualte them
- To create the initial administrator account
*/
error_reporting(0);
include('../admin/adminfuncs.php');
//Grab information from submission
$db_host = $_POST['dbhost'];
$db_user = $_POST['dbuser'];
$db_pass = $_POST['dbpass'];
$db_name = $_POST['dbname'];

//If user has submitted the create account page, run this section
if(isset($_POST['userid']))
{
	createuser();
}
else {
//Create database connection
$conn = @new mysqli($db_host, $db_user, $db_pass, $db_name);

if (mysqli_connect_errno())
{
	echo 'Error connecting to database.<br><br>The credentials entered were incorrect.  Please check the credentials and try again.';
	echo '<br><a href="install.php">Return to Credential Enter page</a>';
	exit;
}


//Create config file
$filecontents = '<?php
$db_host = '.$db_host.';
$db_user = '.$db_user.';
$db_pass = '.$db_pass.';
$db_name = '.$db_name.';
?>';

$cfile = fopen("../inc/config.php", "w"); //wipe if exists
fwrite($cfile, $filecontents);
fclose($cfile);

/* Create database tables:
Existing tables with the same name are dropped, and new tables are created.  Some have populated data to start
*/
$query = "drop table if exists orders";
$result = $conn->query($query);

if (!$result) {
	db_fail(dorders);
	exit;
}

$query = "create table orders(ordernum int unsigned not null auto_increment primary key, firstname varchar(30) not null, lastname varchar(30) not null, homeroomteacher varchar(30) not null, homeroomnumber int unsigned not null, grade int unsigned not null, emailadd varchar(50) not null, phone varchar(20) not null, bookquantity int unsigned not null, personalname varchar(29), person_icon1 int unsigned, person_icon2 int unsigned, person_icon3 int unsigned, person_icon4 int unsigned, price int unsigned not null, date varchar(20) not null, cardnum int unsigned not null, cardtype varchar(5) not null, cardname varchar(50) not null, orderstatus varchar(20), orderupdate varchar(20), orderdateupdate varchar(20))";
$result = $conn->query($query);

if (!$result)
{
	db_fail(corder);
	exit;
}

$query = "drop table if exists mfh";
$result = $conn->query($query);

if (!$result)
{
	db_fail(dmfh);
	exit;
}

$query = "create table mfh(firstname varchar(30) not null, lastname varchar(30) not null, homeroom varchar(30) not null, emailadd varchar(50) not null, phone varchar(20) not null, date varchar(20) not null, message text not null, picture varchar(100) not null UNIQUE KEY, cardnum int unsigned not null, cardtype varchar(5) not null, cardname varchar(50) not null)";
$result = $conn->query($query);

if (!$result)
{
	db_fail(cmfh);
	exit;
}

$query = "drop table if exists settings";
$result = $conn->query($query);

if (!$result)
{
	db_fail(dsettings);
	exit;
}

$query = "create table settings(setname varchar(30) not null primary key, setval varchar(255))";
$result = $conn->query($query);

if (!$result)
{
	db_fail(csettings);
	exit;
}

$query = "insert into settings values('Year', ''), ('IconURL', ''), ('Book_Price', ''), ('Icon_Price', ''), ('Orders_Open', 'no'), ('MFH_Open', 'no'), ('MFH_Price', ''), ('Distribution_Open', 'no'), ('Email_Receipts', 'yes')";
$result = $conn->query($query);

if (!$result)
{
	db_fail(fsettings);
	exit;
}

$query = "drop table if exists icons";
$result = $conn->query($query);

if (!$result)
{
	db_fail(dicons);
	exit;
}

$query = "create table icons(icon int unsigned not null primary key, description varchar(30) not null)";
$result = $conn->query($query);

if (!$result)
{
	db_fail(cicons);
	exit;
}

$query = "INSERT INTO icons VALUES (5001, 'Baseball/Softball'), (5002, 'Basketball'), (5003, 'Cheerleading'), (5004, 'Football'),(5005, 'Golf'), (5006, 'Soccer'),(5008, 'Tennis'),(5009, 'Track/Cross Country'),(5010, 'Volleyball'),(5011, 'Academics'),(5012, 'Peace Symbol'),(5013, 'Dance Shoes'),(5014, 'Hockey'),(5015, 'Drama Masks'),(5018, 'Hearts'),(5019, 'Music'),(5021, 'Smile'),(5022, 'Journalism'),(5023, 'Yin Yang'),(5076, 'Sophomore'),(5130, 'Wrestling'),(5177, 'Freshman'),(5236, 'Graduation Cap'),(5238, 'Band'),(5239, 'Choir'),(5240, 'Painter\'s Palette'),(5241, 'Lacrosse'),(5242, 'Equestrian'),(5246, 'Skateboarding'),(5247, 'Wolf'),(5248, 'Dolphin'),(5253, 'Butterfly'),(5255, 'Star & Crescent'),(5256, 'Star of David'),(5257, 'Cross'),(5258, 'Swimming'),(5360, 'Field Hockey'),(5361, 'Snowboarding'),(5362, 'Martial Arts'),(5363, 'Bulldog'),(5364, 'Pawprint'),(5365, 'Eagle'),(5366, 'Maple Leaf'),(5367, 'American Flag'),(5974, 'Senior'),(5975, 'Junior'),(5976, 'Crew'),(5977, 'Diving'),(5978, 'Fencing'),(5979, 'Figure Skater'),(5980, 'Flag/Color Guard'),(5981, 'Motocross'),(5982, 'Photography'),(5983, 'Surfer'),(9001, 'Weightlifting'),(9006, 'Gymnastics'),(9007, 'Danceline'),(9025, 'Bowling'),(9028, 'Clover Leaf'),(9036, 'Rose'),(9037, 'Palm Trees'),(9042, 'Dragon'),(9050, 'Guitar'),(9051, 'Deer/Elk'),(9073, 'Ram'),(9074, 'Lion'),(9078, 'Bull')";
$result = $conn->query($query);

if (!$result)
{
	db_fail(ficons);
	exit;
}

$query = "drop table if exists announcements";
$result = $conn->query($query);

if (!$result)
{
	db_fail(dannounce);
	exit;
}

$query = "drop table if exists users";
$result = $conn->query($query);

if (!$result)
{
	db_fail(dusers);
	exit;
}

$query = "create table users(uid int unsigned not null auto_increment primary key, username varchar(50) not null unique key, pass varchar(255) not null, name varchar(50) not null, email varchar(50) not null, creationdate varchar(20) not null, acctype int unsigned not null, accstatus int unsigned not null, lastlogin varchar(20) not null, sessionid varchar(255))";
$result = $conn->query($query);

if (!$result)
{
	db_fail(cusers);
	exit;
}

$query = "create table announcements(annid int unsigned not null auto_increment primary key, uid int unsigned not null, date varchar(20) not null, subject varchar(50) not null, message text not null, FOREIGN KEY (uid) REFERENCES users(uid))";
$result = $conn->query($query);

if (!$result)
{
	db_fail(cannounce);
	exit;
}

$query = "drop table if exists logs";
$result = $conn->query($query);

if (!$result)
{
	db_fail(dlogs);
	exit;
}

$query = "create table logs(lid int unsigned not null auto_increment primary key, user varchar(50) not null, action varchar(255) not null, type varchar(50) not null, date varchar(20) not null)";
$result = $conn->query($query);

if (!$result)
{
	db_fail(clogs);
	exit;
}

echo "<h1>Yearbook Business System Installer - Database Creation - Step 2</h1>";
$conn->close();

/* Check Configuration File
This section attempts to connect to the database with the new database configuration file, to verify that it was written correctly
*/

include ('../inc/config.php');
$conn = @new mysqli($db_host, $db_user, $db_pass, $db_name);
if (mysqli_connect_errno())
{
        echo "Error connecting to database with generated configuration file.  Check to make sure the configuration file was created appropriately and run the installer again.";
        echo '<br><a href="install.php">Return to Credential Enter page</a>';
        exit;
}

//Log success of database table setup
ybk_logger($conn, "install", "Database Tables Created Successfully", "Installation");

//Set Up User Account
promptuser();
}

$conn->close();

/* db_fail
This function is called if there is an error in setting up the database.  It will print out the relevant error statement and then quit
@param: Action that caused error
*/
function db_fail($error)
{
	if ($error == dorders) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Orders Table'";
	}

	else if ($error == corder) {
		echo "<br><br>Database Setup failed at step 'Create Order Table'";
	}

	else if ($error == dmfh) {
		echo "<br><br>Database Setup failed at step 'Drop Existing MFH table'";
	}

	else if ($error == cmfh) {
		echo "<br><br>Database Setup failed at step 'Create MFH Table'";
	}

	else if ($error == dsettings) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Settings table'";
	}

	else if ($error == csettings) {
		echo "<br><br>Database Setup failed at step 'Create Settings Table'";
	}

	else if ($error == fsettings) {
		echo "<br><br>Database Setup failed at step 'Fill Settings Table'";
	}

	else if ($error == dicons) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Icons table'";
	}

	else if ($error == cicons) {
		echo "<br><br>Database Setup failed at step 'Create Icons Table'";
	}

	else if ($error == ficons) {
		echo "<br><br>Database Setup failed at step 'Fill Icons Table'";
	}
	else if ($error == dusers) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Users table'";
	}

	else if ($error == cusers) {
		echo "<br><br>Database Setup failed at step 'Create Users Table'";
	}

	else if ($error == adduser) {
		echo "<br><br>Database Setup failed at step 'Add site administrator account'";
	}

	else if ($error == dannounce) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Annoucements Table'";
	}

	else if ($error == cannounce) {
		echo "<br><br>Database Setup failed at step 'Create Annoucements Table'";
	}

	else if ($error == dlogs) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Logs Table'";
	}

	else if ($error == clogs) {
		echo "<br><br>Database Setup failed at step 'Create Logs Table'";
	}
	
	else {
		echo "<br><br>Unsure as to why we reached this point!"; //should never run
	}
}

/*promptuser
This function will display the form to create the site adminstrator account
*/
function promptuser()
{
	echo "<h3>Create site administrator account</h3>This account will have full permissions to the administrator console and cannot be deleted or locked.  Please enter user info below:";
	echo "<br><br><form action=\"install2.php\" method=\"post\">Your name: <input type=\"text\" name=\"name\" required>";
	echo "<br>Your email: <input type=\"email\" name=\"email\" required><br>Username: <input type=\"text\" name=\"userid\" required><br>Password: <input type=\"password\" name=\"pass1\" id=\"pass1\" required><br>Re-enter Password: <input type=\"password\" name=\"pass2\" id=\"pass2\" required>";
	echo "<br><input type=\"submit\" value=\"Submit Info\" onclick=\"return checkpw();\" ></form>";
}

/*createuser
This form will create the user account using the details submitted through promptuser*/
function createuser()
{
	include ('../inc/dbfuncs.php');
	$conn = dbconn();
	$name = $_POST['name'];
	$email = $_POST['email'];
	$userid = $_POST['userid'];
	$pass = sha1($_POST['pass1']);
	$pass2 = sha1($_POST['pass2']);
	$date = date('Y-m-d H:i:s');

	if($pass != $pass2)
	{
		echo "Your passwords don't match, try again";
		promptuser();
	}

	$query = "select * from users";
	$result = $conn->query($query);
	if($result->num_rows != 0)
	{
		echo "A user already exists!  Check database and re-run installer";
		exit;
	}


	$query = "insert into users (username, pass, name, email, creationdate, acctype) values('$userid', '$pass', '$name', '$email', '$date', 3)";
	$result = $conn->query($query);
	if (!$result)
	{
		db_fail(adduser);
		exit;
	}
	ybk_logger($conn, "install", "Super Admin account \"".$userid."\" created", "Installation");
	echo "<h1>Database Creation and Installation Completed Successfully</h1>";
	echo "<h2>User added and activated.  Welcome.</h2><br><a href=\"../admin/login.php\">Log into administrator console to continue setup</a>";
}

?>
</body>
</html>

<script type="text/javascript">
//checkpw
//This function checks if the two passwords match, and will not allow a submission if they do not
function checkpw() {
	var pass1 = document.getElementById('pass1').value;
	var pass2 = document.getElementById('pass2').value;
	if(pass1 != pass2)
	{
		alert('Your passwords do not match.  Please re-validate');
		return false;
	}
	else
		return true;
}
</script>
