<?php

include ('inc/dbfuncs.php');
$conn = dbconn(); // Page will error if not installed

?>
<html>
<head>
<title>Welcome to the Yearbook Business System</title>
<link rel="stylesheet" href="inc/global.css">
</head>
<body>
<h1>Welcome to the Yearbook Business System</h1>
<hr>
<p>Please select the appropriate location you wish to visit from the list below:</p>
<ul><li>Looking to purchase a book? <a href="order/order.php">Click here to place an order</a></li>
<li>Looking to order a Message from Home? <a href="mfh/mfh.php">Click here to place a MFH order</a></li>
<li>Staff member looking to log-in? <a href="admin/login.php">Click here</a></li></ul>
</body>
</html>
