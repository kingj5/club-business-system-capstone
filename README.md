# README #

This repository is for my SUNY Polytechnic Institute Capstone project, which is to set up a business system for a Yearbook club.  Full information regarding the project can be found in the Project Proposal PDF found in the repository's doc folder, along with the project's User Manual.

## Current Version Information ##

* Version 1.1
* Currently Implemented Features: 
    * Installer
    * Order Pages 
    * MFH Order pages
    * Admin panel with access to:
         * Creating accounts
         * Adjusting your own account settings
         * Adjusting system settings for operation of the system
         * Search Order/Distribution module to allow for orders to be marked as distributed.
         * Admin ability to edit or delete an order.
         * Ability for admin to edit previously created accounts
         * Ability for admins and editors to leave announcements on the dashboard
         * Ability for all members with access to view MFH submissions
         * Ability for Editors and Administrators to leave system announcemnets for all users
         * General system statistics displayed on dashboard
         * Report viewer for viewing full information of those who have placed orders, those who have ordered Messages from Home, and those who have ordered personalization options with their book.
         * Logging of most actions in the admin panel, allowing for an administrator to view when specific actions took place and who took said actions.
         * Email receipt capability, with toggle to disable for orders.
         * Account creation information sent to new owner when account is created for them.

## How do I get set up? ##

System Requirements are as follows:

* Web server with PHP5 and the mysqli extension (the standard mysql extension was deprecated as of PHP version 5.5.0 and is not compatible with this system)
* Decent amount of server storage space to ensure there is enough room for uploaded images to be stored
* MySQL database on a MySQL server.

Installation of this project is quick and simple:

* Download the repository contents - you can either clone the repository via git or download the repository using the Downloads link on the left side of the screen.
* Unpack the repository contents to your web directory.
* Create a MYSQL database on your chosen database server along with a database user account that can access and modify the contents of the database.  Note down this database information for later in the installation process.
* In your web browser, navigate to the project's root directory, followed by "/install/install.php".  This will begin the installation process.
* Enter in your database information as prompted.  The installer will then check to make sure that it can access the database, and then will proceed to write out a configuration file and set up the appropriate tables in the database.
* When prompted, enter the user information for the site administrator account.  This account will be used for logging into the admin panel to modify settings along with view admin information.

Once the installation is complete, use the link provided to log into the admin panel and set up system settings as appropriate.

## Repository Structure and Development Information ##

This repository will be composed mostly of PHP files.  These will need to be executed on a web server with PHP installed.  A SQL database will also be required; this can be hosted either on the same server or a different one.  Installation instructions will set-up the database with the necessary tables.  An installer is included in the /install directory and should be run to set up the database and initial settings.  Further documentation to be included in the Wiki soon.

The current release version is contained within the master branch.  Previous releases will be saved in their own branches by their version number (e.g., version_1.0).  The current stable development version will be contained within the development branch.  Features still in development that are not yet considered stable and are instead a work in progress will be located in their own feature branches.

The machine being used for development is running the contents of this repository on Ubuntu 14.04.02 LTS, with an Apache web server and MySQL database server.

## Developer Information ##

* Jeffrey King: kingj5@sunyit.edu or kingj5@cs.sunyit.edu
