<!DOCTYPE html>
<html>
<head>
<title>Yearbook Business System - MFH Submission</title>
<link rel="stylesheet" href="../inc/global.css">
</head>
</html>

<body>
<?php
/*Message From Home photo upload and submission form
Written by: Jeffrey King
Languages: PHP and HTML
*/

error_reporting(0);

//Establish connection to the database
include('../inc/dbfuncs.php');
$conn = dbconn();

//Get short variable names for submitted content
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$homeroom = $_POST['homeroom'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];
$cardnumb = $_POST['cardnumb'];
$cardtype = $_POST['cardtype'];
$cardname = $_POST['cardname'];
$filedate = date('Y-m-d_H-i-s');
$date = date('Y-m-d H:i:s');
$query = "select setval from settings where setname = 'MFH_Price'";
$result = $conn->query($query);
$row=$result->fetch_assoc();
$price = $row['setval'];
if (!get_magic_quotes_gpc()) 
	$message = addslashes($message);
?>
<h1>Yearbook Business System - Messages From Home Submission</h1>

<?php

//Image validation and upload
//Script adapted from w3schools, http://www.w3schools.com/php/php_file_upload.asp

$target_dir = "../uploads/";
$uploadOk = 1;

// Check file size
if ($_FILES["picture"]["size"] > 2000000) {
    echo "Sorry, your file is too large to be uploaded.  Files can be a max of 2MB.<br>";
    $uploadOk = 0;
}

if ($_FILES["picture"]["size"] == 0 ){
	echo "Either your file is empty or the file is larger than 2MB.  Please try a different file.<br>";
	$uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>Sorry, your file was not uploaded and the form not submitted.  Please correct the above error and try again.";
	exit;
}
$imageFileType = pathinfo(basename($_FILES["picture"]["name"]),PATHINFO_EXTENSION);

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["picture"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        echo "Submitted file is not an image.<br>";
        $uploadOk = 0;
    }
}


// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
    echo "Sorry, only JPG, JPEG & PNG extensions are allowed for images.<br>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>Sorry, your file was not uploaded and the form not submitted.  Please correct the above error and try again.";
	exit;
}
// if everything is ok, try to upload file
else {
	$temp = explode(".",$_FILES["picture"]["name"]);
	$newfilename = $filedate ."_". $firstname.$lastname.".".end($temp);
	if(!move_uploaded_file($_FILES["picture"]["tmp_name"], $target_dir . $newfilename)) {
        echo "<div class=\"errorbox\">Sorry, there was an error uploading your file.  If a resubmission does not work, please contact the system administrator.</div>";
	exit;
    }
}

//Insert data into database
$query = "insert into mfh values ('$firstname', '$lastname', '$homeroom', '$email', '$phone', '$date', \"".$message."\", '$newfilename', '$cardnumb', '$cardtype', '$cardname')";
$result = $conn->query($query);

if (!$result)
{
	echo "<div class=\"errorbox\">Error submitting the data to the database; please try again later.</div>";
	exit;
}


?>
<br>Your message has been successfully submitted!  Below is your receipt.  Please print this page for your records.<br><br>
<?php
$msgbegin = "<br>Your message has been successfully submitted!  Below is your receipt.  Please save this email for your records.<br><br>";
$msg = "Student Name: ".$firstname." ".$lastname."
<br>Homeroom Info: ".$homeroom."
<br>Email: ".$email."
<br>Phone: ".$phone."
<br>Total Paid: $".$price."
<br><br>Submitted message: <br>".stripslashes(nl2br($message))."
<br><br>
<hr>
<br>Thank you for your order!";
echo $msg;

$msg = $msgbegin.$msg;
sendmail($conn, $msg);

/*sendmail
This function will check if email receipts are turned on, and if they are will generate and send out an email receipt to the orderer.
@param: Database Connection
@param: Message to send
*/
function sendmail($conn, $msg) {
        $query = "select setval from settings where setname = 'Email_Receipts'";
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        if ($row['setval'] == "yes") {
                /*Import PHPMailer class
                PHPMailer code is used under the GNU LGPL 2.1 License
                PHPMailer code source: https://github.com/PHPMailer/PHPMailer
                */
                require "../inc/class.phpmailer.php";
                $mail = new PHPMailer;
                $mail->FromName = "Yearbook Business System";
                $mail->addAddress($_POST['email']);
                $mail->isHTML(true);
                $mail->Subject= "Your Yearbook Message From Home Receipt";
                $mail->Body = $msg;
                $mail->send();
        }
}
?>
</body>
</html>
