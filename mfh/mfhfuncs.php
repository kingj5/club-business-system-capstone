<?php
/*Message from Home functions
Written by Jeffrey King
Language: PHP
*/

error_reporting(0);

/* checkopen
This function checks if Messages from Home are permitted to be submitted
@param: Database connection
*/
function checkopen($conn) {
	$query = "select setval from settings where setname = 'MFH_Open'";
	$result = $conn->query($query);
	if(!$result) {
        	echo "<div class=\"errorbox\">Unable to determine if Messages from Home  are open; please contact administrator</div>";
        	exit;
	}
	$row = $result->fetch_assoc();
	if($row['setval'] == 'no')
	{
       		echo "<p>Messages from Home submissions  are not currently open; try again some other time.</p>";
       	 	$open = false;
	}
	else
		$open = true;
	return $open;
}

/* getyear
This function returns the current year from the settings table and echos it for display on the MFH order form
@param: Database connection
*/
function getyear($conn) {
	$query = "select setval from settings where setname = 'Year'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo $row['setval'];
}

/* getprice
This function returns the current price from the settings table and echos it for display on the MFH order form
@param: Database connection
*/
function getprice($conn) {
	$query = "select setval from settings where setname = 'MFH_Price'";
	$result = $conn->query($query);
	$row=$result->fetch_assoc();
	echo $row['setval'];
}
