<!DOCTYPE html>
<html>
<head>
<title>Yearbook Business System - MFH Form</title>
<link rel="stylesheet" href="../inc/global.css">
</head>
<body>

<?php

/*Message From Home Order Form
Written by: Jeffrey King
Languages: PHP, JavaScript, HTML
*/

error_reporting(0);
//Establish connection to the database
include ('../inc/dbfuncs.php');
include ('mfhfuncs.php');
$conn = dbconn();
?>
<h1>Yearbook Business System - Messages From Home Form</h1>
<?php
$open = checkopen($conn);
if (!$open)
	exit;
?>

<p>Messages from home are messages from parents to Seniors printed in the yearbook with a baby photo. The cost is $<?php getprice($conn); ?>.
<p>To order a Message from Home for the <?php getyear($conn);?> book, please fill out the below form and include a baby picture.  A sample message is shown to the right.</p>

<form action="upload.php" method="post" enctype="multipart/form-data">
Student first name: <input type="text" name="firstname" required><img src="samplemfh.png" style="float:right;">
<br>Student last name: <input type="text" name="lastname" required>
<br>Homeroom: <input type="text" name="homeroom" required>
<br>Email: <input type="email" name="email" required>
<br>Phone: <input type="text" name="phone" required>
<br>Photo: <input type="file" name="picture" id="picture" onchange="checksize();"required>
<div id="filesize"></div>
<br><div id="pdisclaimer">Note: Photos should be at least 3 megapixels and cannot be more than 2MB in size</div> <!--2MB limit is default max in php-->
<br>Message Text:
<br><textarea rows="6" cols="60" name="message" required></textarea>
<br><hr>
<h3>Payment Info</h3>
<i>This section has been disabled for the purposes of this project.  In a real system, these would be activated.</i>
<br>Credit Card Number: <input type="text" name="cardnumb" disabled>
<br>Type of Card: <select name="cardtype" disabled><option value="">Select Card Type<option value="mc">MasterCard</option><option value="visa">Visa</option><option value="discover">Discover</option><option value="amex">American Express</option></select>
<br>Name on Card: <input type="text" name="cardname" disabled>
<br><br><input type="submit" name="submit" id="submit"  value="Submit Message">
</form>
</body>
</html>

<script type="text/javascript">
//checksize
//This function checks the file that is selected to be uploaded to determine if the size is greater than the 2MB limit.  If the photo is too large, it will not allow the photo to be submitted until a new photo is swapped in its place.
function checksize()
{
	var file = document.getElementById('picture').files[0];
	var filesize = file.size;
	if (filesize > 2000000) {
		document.getElementById('filesize').className = 'errorbox';
		document.getElementById('filesize').innerHTML="Your file is larger than 2MB, please choose a smaller file";
		document.getElementById('submit').disabled=true; //Do not allow a submission.
	}
	else {
		document.getElementById('filesize').className = '';
		document.getElementById('filesize').innerHTML="";
		document.getElementById('submit').disabled=false;
	}
}
</script>
