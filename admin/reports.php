<?php
/*Report Generation Page
This page will generate reports based on which report the user has selected
Written by: Jeffrey King
Language: PHP
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Reports", $userinfo, $distopen);

if(isset($_POST['report']))
{
	$report = $_POST['report'];
	if ($report == "personalization") {
		echo "<h2 id=\"h2center\">Personalization Report</h2>";
		$query = "select firstname, lastname, personalname, person_icon1, person_icon2, person_icon3, person_icon4 from orders where grade = 12 order by lastname, firstname";
		$result = $conn->query($query);
		if (!$result) {
			echo "<div class=\"errorbox\">Error generating report</div>";
			exit;
		}
		
		$numrows = $result->num_rows;
		if ($numrows == 0) {
			echo "No report to generate";
			exit;
		}
		echo "<div id=\"personaltable\">
		<div class=\"personalrow\">
		<div class=\"studentnameheader\">Student Name</div>
		<div class=\"personalnameheader\">Personalization Name</div>
		<div class=\"icon1header\">Personalization Icon 1</div>
		<div class=\"icon2header\">Personalization Icon 2</div>
		<div class=\"icon3header\">Personalization Icon 3</div>
		<div class=\"icon4header\">Personalization Icon 4</div></div>";
		for ($i = 0; $i < $numrows; $i++) {
			$row = $result->fetch_assoc();
			if (($row['personalname'] != "") || ($row['person_icon1'] != 0 )|| ($row['person_icon2'] != 0 )|| ($row['person_icon3'] != 0) || ($row['person_icon4'] != 0)) {
				$icon1 = $row['person_icon1'];
				$icon2 = $row['person_icon2'];
				$icon3 = $row['person_icon3'];
				$icon4 = $row['person_icon4'];
				echo "<div class=\"personalrow\">
				<div class=\"studentname\">".$row['lastname'].", ".$row['firstname']."</div>
				<div class=\"personalname\">".$row['personalname']."</div>
				<div class=\"icon1\">".getIconInfo($conn, $icon1)."</div>
				<div class=\"icon2\">".getIconInfo($conn, $icon2)."</div>
				<div class=\"icon3\">".getIconInfo($conn, $icon3)."</div>
				<div class=\"icon4\">".getIconInfo($conn, $icon4)."</div></div>";
			}
		}
		echo "</div>";
		ybk_logger($conn, $userinfo, "User generated Personalization Report", "Reports");
	}
	else if ($report == "orders") {
		echo "<h2 id=\"h2center\">Orders Report</h2>";
		$query = "select * from orders order by lastname, firstname";
		$result = $conn->query($query);
		if (!$result) {
			echo "<div class=\"errorbox\">Error generating report</div>";
			exit;
		}
		$numrows = $result->num_rows;
		if ($numrows == 0) {
			echo "No results to display";
			exit;
		}
		echo "<divi class=\"ordertable\"><div class=\"orderrow\">
		<div class=\"tablecell-b\">Name</div>
		<div class=\"tablecell-b\">Grade</div>
		<div class=\"tablecell-b\">Homeroom</div>
		<div class=\"tablecell-b\">Email</div>
		<div class=\"tablecell-b\">Phone Number</div>
		<div class=\"tablecell-b\">Book Quantity</div>
		<div class=\"tablecell-b\">Date Ordered</div>
		<div class=\"tablecell-b\">Personalized</div>
		<div class=\"tablecell-b\">Price</div></div>";
		for ($i = 0; $i < $numrows; $i++) {
			$row = $result->fetch_assoc();
			echo "<div class=\"orderrow\">
			<div class=\"tablecell\">".$row['lastname'].", ".$row['firstname']."</div>
	                <div class=\"tablecell\">".$row['grade']."</div>
        	        <div class=\"tablecell\">".$row['homeroomteacher']." - ".$row['homeroomnumber']."</div>
               		<div class=\"tablecell\">".$row['emailadd']."</div>
                	<div class=\"tablecell\">".$row['phone']."</div>
                	<div class=\"tablecell\">".$row['bookquantity']."</div>
                	<div class=\"tablecell\">".$row['date']."</div>
                	<div class=\"tablecell\">".checkpersonal($conn, $row['personalname'], $row['person_icon1'], $row['person_icon2'], $row['person_icon3'], $row['person_icon4'])."</div>
                	<div class=\"tablecell\">$".$row['price']."</div></div>";		}
	echo "</div>";
	ybk_logger($conn, $userinfo, "User generated Order Submissions Report", "Reports");
	}
	
	else if ($report == "mfh") {
		echo "<h2 id=\"h2center\">Messages From Home Students Report</h2>";	
		$query = "select * from mfh order by lastname, firstname";
		$result = $conn->query($query);
		if (!$result) {
			echo "<div class=\"errorbox\">Error generating report</div>";
			exit;
		}
		$numrows = $result->num_rows;
		if ($numrows == 0) {
			echo "No results to display";
			exit;
		}
		echo "<div class=\"ordertable\">
		<div class=\"orderrow\">
		<div class=\"tablecell-b\">Name</div>
		<div class=\"tablecell-b\">Homeroom</div>
		<div class=\"tablecell-b\">Email</div>
		<div class=\"tablecell-b\">Phone</div>
		<div class=\"tablecell-b\">Date of order</div></div>";
		for ($i = 0; $i < $numrows; $i++) {
			$row = $result->fetch_assoc();
			echo "<div class=\"orderrow\">
			<div class=\"tablecell\">".$row['lastname'].", ".$row['firstname']."</div>
			<div class=\"tablecell\">".$row['homeroom']."</div>
			<div class=\"tablecell\">".$row['emailadd']."</div>
			<div class=\"tablecell\">".$row['phone']."</div>
			<div class=\"tablecell\">".$row['date']."</div></div>";
		}
		echo "</div>";
		ybk_logger($conn, $userinfo, "User generated Message From Home Students Report", "Reports");
	}
	
	else if ($report=="nodistribute") {
		echo "<h2 id=\"h2center\">Orders not yet picked up</h2>";
		$query = "select * from orders where orderstatus <> 'distributed' OR orderstatus IS NULL";
		$result = $conn->query($query);
		if (!$result) {
			echo "<div class=\"errorbox\">Error generating report</div>";
			exit;
		}
		$numrows = $result->num_rows;
		if ($numrows == 0) {
			echo "No results to display";
			exit;
		}
		echo "<div class=\"ordertable\">
		<div class=\"orderrow\">
		<div class=\"tablecell-b\">Name</div>
		<div class=\"tablecell-b\">Grade</div>
                <div class=\"tablecell-b\">Homeroom</div>
                <div class=\"tablecell-b\">Email</div>
                <div class=\"tablecell-b\">Phone Number</div>
                <div class=\"tablecell-b\">Book Quantity</div>
                <div class=\"tablecell-b\">Date Ordered</div>
                <div class=\"tablecell-b\">Personalized</div></div>";
		for ($i = 0; $i < $numrows; $i++) {
                        $row = $result->fetch_assoc();
                        echo "<div class=\"orderrow\">
                        <div class=\"tablecell\">".$row['lastname'].", ".$row['firstname']."</div>
                        <div class=\"tablecell\">".$row['grade']."</div>
                        <div class=\"tablecell\">".$row['homeroomteacher']." - ".$row['homeroomnumber']."</div>
                        <div class=\"tablecell\">".$row['emailadd']."</div>
                        <div class=\"tablecell\">".$row['phone']."</div>
                        <div class=\"tablecell\">".$row['bookquantity']."</div>
                        <div class=\"tablecell\">".$row['date']."</div>
                        <div class=\"tablecell\">".checkpersonal($conn, $row['personalname'], $row['person_icon1'], $row['person_icon2'], $row['person_icon3'], $row['person_icon4'])."</div></div>";
		}
        	echo "</div>";
		ybk_logger($conn, $userinfo, "User generated Orders not picked up report", "Reports");
	}

		

}

else
{
	selectReports();
}





/* Functions */

/*select reports
This function generates the available list of reports that may be displayed
*/
function selectReports() {

	echo "<br>Select the report to generate:
	<form method=\"post\" action=\"reports.php\">
	<br><input type=\"radio\" name=\"report\" value=\"personalization\">Personalization
	<br><input type=\"radio\" name=\"report\" value=\"orders\">All Order Information
	<br><input type=\"radio\" name=\"report\" value=\"nodistribute\">Orders Not Yet Distributed
	<br><input type=\"radio\" name=\"report\" value=\"mfh\">Message From Home Student Information
	<br><br><input type=\"submit\" value=\"Generate Report\"></form>";
}

/*getIconInfo
This function displays the full information about the icon that was selected
@param: Database Connection
@param: Icon ID
@return: Icon information
*/
function getIconInfo($conn, $iconid) {
	if ($iconid == 0) {
		return "No icon";
	}
	else {
		$query = "select description from icons where icon = \"".$iconid."\"";
		$result = $conn->query($query);
		if (!$result) {
			echo "Error";
		}
		$row = $result->fetch_assoc();
		return ($iconid." - ".$row['description']);
	}
}

/*Check if perosonalized
This function checks to see if a book was personalized.  If a book is personalized, it will return a checkmark.  If not, it will return an X
@param: Database Connection
@param: Personalization Name
@params: Icons 1 - 4
@return: Symbol denoting whether personalizations were ordered or not
*/
function checkpersonal($conn, $name, $icon1, $icon2, $icon3, $icon4) {
	if ($name != "" || $icon1 != 0 || $icon2 != 0 || $icon3 != 0 || $icon4 != 0) {
		return "&#10004;";
	}
	else {
		return "&#10008;";
	}
}

createfooter();
?>
