<?php
/*Session Validation
This page will check to see if a user has a valid session.  If they do, they will sent to their appropriate page.  If they do not, they will be given an error and forced to re-login.
Written by: Jeffrey King
Language: PHP
*/
session_start();
error_reporting(0);
/*Session Validation Function
@param: Connection Object
*/
function sesvalid($conn)
{
	if(!isset($_SESSION['uid']) || !isset($_SESSION['sesid']))
	{
        	echo "<!DOCTYPE html>
		<head>
		<title>Yearbook Business System Administration - Access Denied</title>
		<link rel=\"stylesheet\" href=\"../inc/global.css\">
		</head>
		<body>
		<h1>Yearbook Business System - Access Denied</h1><br>
        	Your session has expired.  <a href=\"login.php\">Click here to re-login</a></body></html>";
        	exit;
	}
	$query = "select * from users where uid = \"".$_SESSION['uid']."\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$checksesid = $row['sessionid'];
	if ($checksesid != $_SESSION['sesid'])
	{
        	echo "<!DOCTYPE html>
		<head>
		<title>Yearbook Business System Administration - Access Denied</title>
		<link rel=\"stylesheet\" href=\"../inc/global.css\">
		</head>
		<body>
		<h1>Yearbook Business System - Access Denied</h1><br>
		Access denied - Invalid Session
		<br>This is likely because this account was logged in at another location.<br>
		<br><a href=\"login.php\">Click to re-login</a></body></html>";
		$_SESSION = array();
		session_destroy();
        	exit;
	}
	return $row;

}

?>
