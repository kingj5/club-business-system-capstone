<?php
/*Edit announcement script
This form will edit an announcement with the latest submitted changes
Written by Jeffrey King
Language: PHP
*/
session_start();
error_reporting(0);

include('sesvalid.php');
include('../inc/dbfuncs.php');
include('adminfuncs.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
if (isset($_GET['action']) && isset($_GET['annid']) && $_GET['action'] == "edit") {
        $annid = $_GET['annid'];
        $message = $_POST['message'];
	if (!get_magic_quotes_gpc()) 
		$message = addslashes($message);
        $query = "update announcements set message = \"".$message."\" where annid = \"".$annid."\"";
        $result = $conn->query($query);

        if (!$result) {
                echo "Error updating announcement";
        }
      else {
	ybk_logger($conn, $userinfo, "Edited announcement with ID: ".$annid, "Announcements");
        header('Location: dashboard.php');
	}
}

