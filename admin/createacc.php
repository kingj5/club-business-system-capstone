<?php
/*Create account page
This page allows the administrator to create user accounts
Written by: Jeffrey King
Language: PHP, JavaScript
*/
session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('pagespawn.php');
include('../inc/dbfuncs.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Create New User Account", $userinfo, $distopen);

if($userinfo['acctype'] < 2)
{
	echo "<div class=\"errorbox\">You are not authorized to view this page.</div>";
	exit;
}

if(isset($_POST['username']))
	makeuser($conn, $userinfo);
else
{
	displayform();
}

/*makeuser
This script is run if information is submitted to the page
@param: Database connection
@param: Admin's user information object
*/
function makeuser($conn, $userinfo)
{
	$username = $_POST['username'];
	$newpass1 = sha1($_POST['newpass1']);
	$newpass2 = sha1($_POST['newpass2']);
	$name = $_POST['name'];
	$email = $_POST['email'];
	$role = $_POST['role'];
	$date = date('Y-m-d H:i:s');
	if ($newpass1 != $newpass2)
	{

		echo "<div class=\"errorbox\">Your temporary passwords do not match, please try again.</div>";
		displayform();
		return;
	}
	else if ($role == "")
	{
		echo "<div class=\"errorbox\">You did not select a role; please try again.</div>";
		displayform();
		return;
	}
	else
	{
		$query = "select username from users where username = \"".$username."\"";
		$result = $conn->query($query);
		$numrows = $result->num_rows;
		if ($numrows != 0) {
			echo "<div class=\"errorbox\">The username you have chosen is already in use; please choose another.</div>";
			displayform();
			return;
		}
		$rolenum = 0;
		if ($role == "editor")
		{
			$rolenum = 1;
		}
		if ($role == "admin")
		{
			$rolenum = 2;
		}
		$query = "insert into users(username, pass, name, email, creationdate, acctype, accstatus) values ('$username', '$newpass1', '$name', '$email', '$date', '$rolenum', '1')";
		$result = $conn->query($query);
		if (!$result)
		{
			echo "<div class=\"errorbox\">Entering the data into the users database failed.  Try again later.</div>";
			exit;
		}
		else
		{
			echo "<div class=\"successbox\">Account Created Successfully</div>";
			ybk_logger($conn, $userinfo, "Created account for ".$username, "Administration");
			sendmail();
			echo "<br><a href=\"account.php\">Click to Return to Account Management</a>";
		}
	}
	return;
}

/*Display Form
This function displays the create account form if no user information is entered or invalid user information is entered
*/
function displayform()
{
	echo "<p>Fill out the below form to create a new user</p>
        <form name=\"makeacc\" action=\"createacc.php\" onsubmit=\"return validate()\"method=\"post\">
        User's Name: <input type=\"text\" name=\"name\" required>
        <br>Desired Username: <input type=\"text\" name=\"username\" required>
	<br>Temporary Password: <input type=\"password\" name=\"newpass1\" required>
        <br>Confirm Temporary Password: <input type=\"password\" name=\"newpass2\" required>
        <br>Email: <input type=\"email\" name=\"email\" required>
	<br>Role: <select name=\"role\"><option value=\"\">--Select Role--</option><option value=\"staff\">Staff</option><option value=\"editor\">Editor</option><option value=\"admin\">Admin</option></select>
	<div id=\"roleerr\"></div>
	<br><input type=\"submit\" value=\"Update Info\">
        </form>";
}
/*sendmail
This function will send the new account information to the person who will be holding the account.
*/
function sendmail() {
$msg = "Hi ".$_POST['name'].",<br><br>
An account has been created for you on the Yearbook Business System.<br><br>
Please use the following details to log into the system and finish configuring your account:<br><br>
Username: ".$_POST['username']."<br>
Temporary Password: ".$_POST['newpass1']."<br><br>
You will be prompted to set a new password upon login.";

/*Import PHPMailer class
PHPMailer code is used under the GNU LGPL 2.1 License
PHPMailer code source: https://github.com/PHPMailer/PHPMailer
*/
require "../inc/class.phpmailer.php";
$mail = new PHPMailer;
$mail->FromName = "Yearbook Business System";
$mail->addAddress($_POST['email']);
$mail->isHTML(true);
$mail->Subject= "Account Created on Yearbook Business System";
$mail->Body = $msg;
$mail->send();

}

createfooter();
?>
<script type="text/javascript">
//validate
//This function checks to make sure a role was selected
function validate() {
document.getElementById("roleerr").innerHTML="";
var role = document.forms["makeacc"]["role"].value;
if (role == "") {
	document.getElementById("roleerr").className="errorbox";
	document.getElementById("roleerr").innerHTML="Please Select a Role";
	return false;
}
return true;
}
</script>
