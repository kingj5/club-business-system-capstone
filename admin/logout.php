<?php
/*Logout Script
This script kills an active session then displays the login page
Written by Jeffrey King
Language: PHP, HTML
*/
session_start();
if(empty($_SESSION['sesid']))
{
	header('Location: login.php');
	exit;
}
include('../inc/dbfuncs.php');
$conn = dbconn();
$query = "update users set sessionid = \"NULL\" where uid = \"".$_SESSION['uid']."\"";
$result = $conn->query($query);
$_SESSION = array();
session_destroy();
?>
<html>
<head>
<title>Yearbook Business System - Admin Login</title>
<link rel="stylesheet" href="../inc/global.css">
</head>
<body>
<h1>Yearbook Business System - Admin Login</h1>
<br><div class="successbox">You have successfully logged out.</div>
<form action="validate.php" method="post">
Username: <input type="text" name="username" required>
<br>Password: <input type="password" name="password" required>
<br><input type="submit" value="Submit"></form>
</body>
</html>
