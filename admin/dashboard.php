<?php
/*Dashboard
This page creates the dashboard, along with controlling the announcement functionality
Written by Jeffrey King
Language: PHP, JavaScript
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Dashboard", $userinfo, $distopen);

echo "<div style=\"width: 100%; display: block;\"><div id=\"announcements\">";

//If new announcement
if (isset($_POST['title']) && isset($_POST['message'])) {
	//Insert into the database.
	$date = date('Y-m-d H:i:s');
	$uid = $userinfo['uid'];
	$title = $_POST['title'];
	$message = $_POST['message'];
	if (!get_magic_quotes_gpc()) 
		$message = addslashes($message);
	$query = "insert into announcements (uid, date,subject, message) values ('$uid', '$date', '$title', \"".$message."\")";
	$result = $conn->query($query);
	if (!$result) {
		echo "<div class=\"errorbox\">Error inserting announcement into database</div>";
	}
	else {
		ybk_logger($conn, $userinfo, "Posted announcement on dashboard", "Announcements");
		echo "<div class=\"successbox\">Announcement added successfully</div>";
	}
}

//Delete announcement
else if (isset($_GET['action']) && isset($_GET['annid']) && $_GET['action'] == "delete") {
	$annid = $_GET['annid'];
        $message = $_POST['message'];
        $query = "delete from announcements where annid = \"".$annid."\"";
        $result = $conn->query($query);
        if (!$result) {
                echo "<div class=\"errorbox\">Error deleting announcement</div>";
        }
        else {
		ybk_logger($conn, $userinfo, "Deleted announcement with ID: ".$annid, "Announcements");
                echo "<div class=\"successbox\">Announcement deleted</div>";
        }

}

else {} //continue

if ($userinfo['acctype'] > 0) // not staff
{
	echo "<br><input type=\"button\" onclick=\"mkannounce();\" style=\"float: right;\" value=\"Add Announcement\"><br><br>
	<div id=\"addannounceblock\"></div>";
}

//Display all announcements
displayannouncements($conn, $userinfo);

echo "</div>"; //end announcement block

//Generate stats block
echo "<div id=\"stats\"><h2 id=\"h2center\">System Statistics</h2>
Distribution Status: ";
if ($distopen == "yes") {
	echo "Distribution is Active";
}
else {
	echo "Distribution not active";
}
echo "<br>Order Status: ".getOrderStatus($conn)."
<br>MFH Status: ".getMFHStatus($conn)."
<br>
<br>
Total Orders: ".getOrderTotals($conn)."
<br>Total MFH Orders: ";
$mfhtotal = getMFHTotal($conn);
echo $mfhtotal."
<br><br>
Total Money Accrued: $".getTotalMoney($conn, $mfhtotal)."
</div></div>";

/* Functions */

/*displayannouncements
This function will display all of the current announcements.  It will also determine whether or not announcements should have edit/delete buttons on them based on who posted them and your user information
@param: Database Connection
@param: Account user object
*/
function displayannouncements($conn, $userinfo) {
	$query = "select * from announcements order by annid desc";
	$result = $conn->query($query);
	if (!$result) {
		echo "Error fetching announcements from database";
	}
	else {
		$numrows = $result->num_rows;
		if ($numrows == 0) 
			echo "No announcements to display.";
		for ($i = 0; $i < $numrows; $i++) {
			$row = $result->fetch_assoc();
			$name = getPoster($conn, $row['uid']);
			echo "<div class=\"announce\"><div class=\"annsubject\">".$row['subject']."</div>
			<div class=\"announcepostinfo\">Posted on ".$row['date']." by ".$name."</div>";
			if ($userinfo['uid'] == $row['uid'] || $userinfo['acctype'] > 1) {
				echo "<div class=\"annoptions\"><a href=\"#?annid=".$annid."\" onclick=\"editannounce(".$row['annid'].", 'ann".$row['annid']."'); return false;\">Edit</a> &nbsp; &nbsp; <a href=\"dashboard.php?action=delete&annid=".$row['annid']."\" onclick=\"return deleteconfirm();\">Delete</a></div>";
			}
			echo "<div class=\"annmessage\" id=\"ann".$row['annid']."\">".nl2br($row['message'])."</div><br><br></div>";
		}
	}
}
echo "<div id=\"b4edit\"></div>";
/*getPoster
This function is used to get the username of the person who posted an announcement and returns it
@param: Databse Connection
@param: user's id number
@return: User's name
*/
function getPoster($conn, $user) {
	$query = "select name from users where uid = \"".$user."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "Error grabbing user";
	}
	else {
		$row=$result->fetch_assoc();
		return $row['name'];
	}
}

/*getOrderStatus
This determines whether orders are open or not and returns the appropriate string
@return: String of order status
*/
function getOrderStatus($conn) {
	$query = "select setval from settings where setname = \"Orders_Open\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "Error generating stats";
	}
	else {
		$row = $result->fetch_assoc();
		if ($row['setval'] == "no")
			return "Orders not currently open";
		else
			return "Orders are open";
	}
}

/*getMFHStatus
This determines whether MFH orders are open or not and returns the appropriate string
@return: String of MFH order status
*/
function getMFHStatus($conn) {
	$query = "select setval from settings where setname = \"MFH_Open\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "Error generating stats";
	}
	else {
		$row = $result->fetch_assoc();
		if ($row['setval'] == "no")
			return "Messages from Home orders are not currently open";
		else
			return "Messages from Home orders are open";
	}
}

/*getOrderTotals
This function determines all of the order totals per grade, as well as all orders in general and generates a string to return
@param: Database Connection
@return: String of order total information
*/
function getOrderTotals($conn) {
	$query = "select bookquantity from orders";
	$result = $conn->query($query);
	if (!$result) {
                echo "Error generating stats";
        }
	else {
		$num_rows = $result->num_rows;
		$sum = 0;
		for($i = 0; $i < $num_rows; $i++) {
			$row = $result->fetch_assoc();
			$sum += $row['bookquantity'];
		}
		$grade9 = getBookTotalsByGrade($conn, 9);
		$grade10 = getBookTotalsByGrade($conn, 10);
		$grade11 = getBookTotalsByGrade($conn, 11);
		$grade12 = getBookTotalsByGrade($conn, 12);
		$returnval = $num_rows."
		<br>Books sold in grade 9: ".$grade9."
		<br>Books sold in grade 10: ".$grade10."
		<br>Books sold in grade 11: ".$grade11."
		<br>Books sold in grade 12: ".$grade12."
		<br>Total Books Sold: ".$sum;
		return $returnval;
	}
}

/*getBookTotalsByGrade
This function gets the book quantities by grade
@param: Database Connecton
@param: Grade
@return: integer of number of books per grade
*/
function getBookTotalsByGrade($conn, $grade) {
	$query = "select bookquantity from orders where grade = \"".$grade."\"";
	$result = $conn->query($query);
	$num_rows = $result->num_rows;
	$sum = 0;
	for ($i = 0; $i < $num_rows; $i++) {
		$row = $result->fetch_assoc();
		$sum += $row['bookquantity'];
	}
	return $sum;
}

/*getMFHTotal
This function returns the number of MFH orders submitted
@param: Database Connection
@return: integer containing number of orders
*/
function getMFHTotal($conn) {
	$query = "select * from mfh";
	$result = $conn->query($query);
	if (!$result) {
                echo "Error generating stats";
        }
	else {
		return $result->num_rows;
	}
}

/*getTotalMoney
This function determines the amount of money that the system has brought in so far after calculating the prices of all orders
@param: Database Connection
@param: Number of MFH orders submitted
@return: Total amount of money made
*/
function getTotalMoney($conn, $mfh) {
	$total = 0;
	$query = "select price from orders";
	$result = $conn->query($query);
        if (!$result) {
                echo "Error generating stats";
		exit;
        }

	$num_rows = $result->num_rows;
        for($i = 0; $i < $num_rows; $i++) {
        	$row = $result->fetch_assoc();
              	$total += $row['price'];
		}
	$query = "select setval from settings where setname = \"MFH_Price\"";
        $result = $conn->query($query);
        if (!$result) {
                echo "Error generating stats";
                exit;
        }
	$row = $result ->fetch_assoc();
	$total += ($mfh * $row['setval']);
	return $total;
}
	
?>

<script type="text/javascript">
//Make Announcement
//This function populates a div with the the form to create a new announcement
function mkannounce() {
	document.getElementById('addannounceblock').innerHTML="<div id=\"mkanntitle\">Create Announcement</div><br> \n<form method=\"post\" action=\"dashboard.php\">\nAnnouncement Title: <input type=\"text\" length=\"50\" name=\"title\" required>\n<br><textarea rows=\"6\" cols=\"60\" name=\"message\" placeholder=\"Place your Announcement Text Here\" required></textarea>\n<br><input type=\"submit\" value=\"Submit\"> &nbsp; <input type=\"button\" onclick=\"rmmkannounce();\" value=\"Cancel\"></form>";
}

//Remove Make Announcement
//If the cancel buttion in the create announcement block is clicked, this will remove the form from the div
function rmmkannounce() {
	document.getElementById('addannounceblock').innerHTML="";
}

//Edit Announcement
//If the edit button is clicked, replace the announcement with an edit block to edit the announcement
function editannounce(annid, divid) {
	var message = document.getElementById(divid).innerHTML;
	document.getElementById('b4edit').innerHTML=message;
	message = message.replace(/<br>/g, '');
	document.getElementById(divid).innerHTML="<form method=\"post\" action=\"editannounce.php?action=edit&annid=" + annid + "\"><textarea rows=\"6\" cols=\"60\" name=\"message\" placeholder=\"Place your Announcement Text Here\" required>" + message + "</textarea><br><input type=\"submit\" value=\"Submit Edit\"> &nbsp; <input type=\"button\" onclick=\"rmeditannounce('"+divid+"');\" value=\"Cancel\"></form>";
}

//Remove Edit Annoumcenet
//If the cancel buttion is clicked, replace the edit block with the original text that was contained within the announcement
function rmeditannounce(divid) {
	document.getElementById(divid).innerHTML= document.getElementById('b4edit').innerHTML;
}

//Delete Confirmation
//Force a confirmation box to check if the user is sure they want to delete their announcement
function deleteconfirm() {
	return confirm('Are you sure you want to delete this announcement?');
}
</script>

<?php
createfooter();
?>
