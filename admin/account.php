<?php
/*Manage Account Page
This page provides options for managing an account
Written by: Jeffrey King
Language: PHP
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('pagespawn.php');
include('../inc/dbfuncs.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Account Management", $userinfo, $distopen);

?>

<p>Manage Your Account:</p>
<ul><li><a href="passchange.php">Change Password</a></li>
<li><a href="emailchange.php">Change Email</a></li></ul>

<?php
if ($userinfo['acctype'] >= 2)
{
	echo "<p>Administrate Accounts:</p>";
	echo "<ul><li><a href=\"viewaccounts.php\">View All Accounts</a></li>";
	echo "<li><a href=\"createacc.php\">Create New Account</a></li></ul>";
}
createfooter();
?>
