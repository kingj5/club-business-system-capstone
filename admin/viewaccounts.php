<?php
/*View Accounts
This page will display accounts with access to the administration panel and will allow an admin to perform basic account adjustments to the account
Written by Jeffrey King
Language: PHP, JavaScript
*/
session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("View Accounts", $userinfo, $distopen);

if($userinfo['acctype'] < 2) {
	echo "<div class=\"errorbox\">Access denied - you do not have permission to view this page.</div>";
	exit;
}
// If we are attempting to take action on an account
if(isset($_GET['accnum']) && isset($_GET['action'])) {
	switch ($_GET['action']) {
		case "temppass":
			temppass($conn, $_GET['accnum']);
			break;
		case "changerole":
			changerole($conn, $_GET['accnum']);
			break;
		case "deleteuser":
			deleteuser($conn, $userinfo, $_GET['accnum']);
			break;
		default:
			createacctable($conn);
	}
}
// Assign Temporary Password section
else if (isset($_POST['pass1'])) {
	if ($_POST['pass1'] != $_POST['pass2']) {
		echo "<div class=\"errorbox\">Passwords do not match.  Try again.</div><br>";
		temppass($conn, $_GET['accnum']);
		exit;
	}
	$pass1 = SHA1($_POST['pass1']);
	$query = "update users set pass = \"".$pass1."\", accstatus = 1 where uid = \"".$_GET['accnum']."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "<div class=\"errorbox\">Error updating database.</div>";
		exit;
	}
	ybk_logger($conn, $userinfo, "Assigned temporary password for ".getAccName($conn, $_GET['accnum']), "Administration");
	echo "<div class=\"successbox\">Temporary password for ".getAccName($conn, $_GET['accnum'])." created successfully</div><br>";
	createacctable($conn);
}
// Assign New Role section
else if (isset($_POST['role'])) {
	$query = "update users set acctype = \"".$_POST['role']."\" where uid = \"".$_GET['accnum']."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "<div class=\"errorbox\">Error updating database.</div>";
		exit;
	}
	ybk_logger($conn, $userinfo, "Account role for ".getAccName($conn, $_GET['accnum'])." updated to ".getRoleName($_POST['role']), "Administration");
	echo "<div class=\"successbox\">Account role for ".getAccName($conn, $_GET['accnum'])." edited successfully</div><br>";
	createacctable($conn);
}

else
	createacctable($conn, $userinfo);

/*Create Account Table
This function will create the list of all accounts with access to the system and their relevant options
@param: Database Connection
@param: User Account Information Object
*/
function createacctable($conn, $userinfo) {
	echo "<h2>List of all accounts on system</h2>";
	$query = "select * from users";
	$result = $conn->query($query);
	if (!$result) {
		echo "<div class=\"errorbox\">Error generating table</div>";
		exit;
	}
	$rownum = $result->num_rows;
	echo "<div id=\"acctable\">
	<div class=\"acctablerow\">
	<div class=\"usernum\">User Number</div>
	<div class=\"username\">Username</div>
	<div class=\"name\">Display Name</div>
	<div class=\"role\">User Role</div>
	<div class=\"useroption\">Edit User Options</div>
	</div>
	";

	for ($i = 0; $i < $rownum; $i++) {
		$row = $result->fetch_assoc();
		if ($row['accstatus'] == 2) //account is closed
			{} //do not print, skip
		else { //all other accounts
			echo "<div class=\"acctablerow\">
			<div class=\"usernum\">".$row['uid']."</div>
			<div class=\"username\">".$row['username']."</div>
			<div class=\"name\">".$row['name']."</div>
			<div class=\"role\">".getAccType($row)."</div>
			<div class=\"useroption\">".getOptions($row, $userinfo)."
			</div></div>";
		}
	}
	echo "</div>";

}
/*Get Account Type function
This function will get the integer role value and pass it on to the role name function 
@param: result set
@return: Role Name
*/
function getAccType($row) {
	$role = $row['acctype'];
        return getRoleName($role);
}

/*Get Role Name Function
This function will return the string format of the appropriate role
@param: Role number
@return: String version of role
*/
function getRoleName($role) {
	if ($role ==0)
                return "Staff Member";
        else if ($role == 1)
                return "Editor";
        else if ($role == 2)
                return "Administrator";
        else if ($role == 3)
                return "Super Admin";
        else
                exit;
}

/*get options
This function will set up the appropriate editing options for each account.
@param: row from database
@param: User Account Information Object
@return: HTML for generating the appropriate options for each account
*/
function getOptions($row, $userinfo) {
	if ($row['acctype'] == 3)
		return "<ul><li>Can't edit Super Admin</li></ul>";
	else if ($row['uid'] == $userinfo['uid']) {
		return "<ul><li>Can't edit own account here - use regular accounts menu</li></ul>";
	}
	else {
		return "<ul><li><a href=\"viewaccounts.php?accnum=".$row['uid']."&action=temppass\">Assign Temporary Password</a></li>
        	<li><a href=\"viewaccounts.php?accnum=".$row['uid']."&action=changerole\">Change Role</a></li>
        	<li><a href=\"viewaccounts.php?accnum=".$row['uid']."&action=deleteuser\" onclick=\"return confirmdelete();\">Close User Account</a></li></ul>";
	}
}

/*get account name
This function returns the account's name
@param: Database Connection
@param: User ID Number
@return: User's name
*/
function getAccName($conn, $user) {
	$query = "select name from users where uid = \"".$user."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "An error occurred.";
		exit;
	}
	$row = $result->fetch_assoc();
	return $row['name'];
}

/*temporary password form
This function displays the form needed to create a temporary password for a user
@param: Database Connection
@param: User ID Number
*/
function temppass($conn, $user) {
	echo "Use this form to set a temporary password for account belonging to ".getAccName($conn, $user).":
	<form method=\"post\" action=\"viewaccounts.php?accnum=".$user."\">
	<br>Temporary Password: <input type=\"password\" name=\"pass1\" required>
	<br>Repeat Temporary Password: <input type=\"password\" name=\"pass2\" required>
	<br><input type=\"submit\" value=\"Submit\"></form>";
}

/*Generate Role Options
This function will generate the role options dropdown in the edit roles page.  The current role will be the default option
@param: Database Connection
@param: User ID Number
@return: Options list
*/
function generateRoleOptions($conn, $user) {
	$query = "select acctype from users where uid = \"".$user."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "Error generating role list.";
		exit;
	}
	$row = $result->fetch_assoc();
	$userrole = $row['acctype'];
	if ($userrole == 0) {
		return "<option value=\"0\" selected=\"selected\">Staff Member</option>
			<option value=\"1\">Editor</option>
			<option value=\"2\">Administrator</option>";
	}
	else if ($userrole == 1) {
		return "<option value=\"0\">Staff Member</option>
			<option value=\"1\" selected=\"selected\">Editor</option>
			<option value=\"2\">Administrator</option>";
	}
	else if ($userrole == 2) {
		return "<option value=\"0\" selected=\"selected\">Staff Member</option>
			<option value=\"1\">Editor</option>
			<option value=\"2\" selected=\"selected\">Administrator</option>";
	}
	else {}
}

/*Display Change Role form
This function displays the change role form
@param: Database Connection
@param: User ID Number
*/
function changerole($conn, $user) { 
	echo "Use this form to change the account role for ".getAccName($conn, $user).".
	<form method=\"post\" action=\"viewaccounts.php?accnum=".$user."\">
	<br>New Role: <select name=\"role\">".generateRoleOptions($conn, $user)."</select>
	<br><input type=\"submit\" value=\"Submit\"></form>";
}

/*Delete User function
This function will close a user account
@param: Database Connection
@param: User Account Information Object
@param: User ID Number
*/
function deleteuser($conn, $userinfo, $user) {
	$query = "update users set accstatus = \"2\" where uid = \"".$user."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "<div class=\"errorbox\">Error updating database.</div>";
		exit;
	}
	ybk_logger($conn, $userinfo, "Account for ".getAccName($conn, $user)." closed", "Administration");
	echo "<div class=\"successbox\">Account for ".getAccName($conn, $user)." deleted successfully.</div><br>";
	createacctable($conn);
}

createfooter();
?>
<script type="text/javascript">
//Confirm Account Closure
//This function displays a confirmation message to make sure you wish to close the account
function confirmdelete() {
	return confirm("Are you sure you wish to close this account?");
}
</script>
