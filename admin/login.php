<?php
/*Login Page
Written by Jeffrey King
Language: PHP, HTML
*/
session_start();
//If we have already logged in, redirect to dashboard
if(isset($_SESSION['sesid']))
{
	header('Location: dashboard.php');
	exit;
}
session_destroy(); //shouldn't have a session when at a login page
?>
<html>
<head>
<title>Yearbook Business System - Admin Login</title>
<link rel="stylesheet" href="../inc/global.css">
</head>
<body>
<h1>Yearbook Business System - Admin Login</h1>
<br>
<form action="validate.php" method="post">
Username: <input type="text" name="username" required>
<br>Password: <input type="password" name="password" required>
<br><input type="submit" value="Submit"></form>
</body>
</html>
