<?php
/*Change User Password
This script contains functions to change a user's password
Written by: Jeffrey King
Language: PHP
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('pagespawn.php');
include('../inc/dbfuncs.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Change User Password", $userinfo, $distopen);

//If user has submitted a password change request
if(isset($_POST['oldpass']))
	pwupdate($conn, $userinfo);
else
{
	changepass();
}

/*password update
This function will validate the password change request and update the database
@param: Database Connection
@param: User Account Information Object
*/
function pwupdate($conn, $userinfo)
{
	$oldpass = sha1($_POST['oldpass']);
	if ($oldpass != $userinfo['pass'])
	{
		echo "<div class=\"errorbox\">Your Current Password does not match the one on file, please try again.</div>";
		changepass();
		return;
	}
	$newpass1 = sha1($_POST['newpass1']);
	$newpass2 = sha1($_POST['newpass2']);
	if ($newpass1 != $newpass2)
	{

		echo "<div class=\"errorbox\">Your new passwords do not match, please try again.</div>";
		changepass();
		return;
	}
	else
	{
		$query = "update users set pass=\"".$newpass1."\" where uid = \"".$userinfo['uid']."\"";
		$result = $conn->query($query);
		if (!$result)
		{
			echo "<div class=\"errorbox\">There was an error updating the users database.  Try again later.</div>";
			exit;
		}
		else
		{
			ybk_logger($conn, $userinfo, "User updated own password", "Account");
			echo "<div class=\"successbox\">Password Updated Successfully!</div>";
			echo "<br><a href=\"account.php\">Click to Return to Account Management</a>";
		}
	}
	return;
}

/*Change Password function
This function will display the change password form to be filled out
*/
function changepass()
{
	echo "<p>Fill out the below form to change your password</p>
        <form action=\"passchange.php\" method=\"post\">
        Current Password: <input type=\"password\" name=\"oldpass\" required>
        <br>New Password: <input type=\"password\" name=\"newpass1\" required>
        <br>Confirm New password: <input type=\"password\" name=\"newpass2\" required>
        <br><input type=\"submit\" value=\"Update Info\">
        </form>";
}

createfooter();
?>
