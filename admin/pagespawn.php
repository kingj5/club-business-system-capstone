<?php
/*Page Spawner Tool
This script contains functions that will generate the header, navigation links, and footer
Written by: Jeffrey King
Language: PHP
*/

session_start();
error_reporting(0);

/*Create Header
This function creates the header that appears at the top of every page
@param: Page Title
@param: User's account information object
@param: Distribution Status
*/
function createheader($pagetitle, $userinfo, $distopen) {
	$role = getrole($userinfo);
	echo "<!DOCTYPE html>
<head>
<title>Yearbook Business System Administration - ".$pagetitle."</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"admin.css\">
<link rel=\"stylesheet\" href=\"../inc/global.css\">
</head><body>";
	echo "<h1>Yearbook Business System Administration - ".$pagetitle."</h1>
	<div id=\"loginas\" style=\"float:right;\">Welcome, ".$userinfo['name']."<br>Role: ".$role."</div>
	<div id=\"nav\"><ul>";
	createnav($userinfo, $distopen);
	echo "</ul></div><br><br><hr>";
}

/*Get User's Role
This function will get a user's role in text, given the user's role in integer format
@param: User information object
@return: String containing user's role
*/
function getrole($userinfo) {
	$role = $userinfo['acctype'];
	if ($role ==0)
		return "Staff Member";
	else if ($role == 1)
		return "Editor";
	else if ($role == 2)
		return "Administrator";
	else if ($role == 3)
		return "Super Admin";
	else
		exit;
}

/*Create navigation Links
This function will display the appropriate navigation links for each role
@param: User's Account Information Object
@param: Distribution Status
*/
function createnav($userinfo, $distopen) {
	$role = $userinfo['acctype'];
	echo "<li><a href=\"dashboard.php\">Dashboard</a></li>";
	if($distopen == "yes")
		echo "<li><a href=\"searchorder.php\">Distribute Books</a></li>";
	else if($role == 2 || $role == 3)
		echo "<li><a href=\"searchorder.php\">Search Orders</a></li>";
	else
		{}//continue;
	echo "<li><a href=\"mfhviewer.php\">View Messages From Home Submissions</a>
	<li><a href=\"reports.php\">Generate Reports</a></li>";
	if ($role == 2 || $role == 3)
		echo "<li><a href=\"viewlogs.php\">View System Logs</a></li>";
	echo "<li><a href=\"account.php\">Account</a></li>";
	if($role == 2 || $role == 3)
		echo "<li><a href=\"settings.php\">Settings</a></li>";
	echo "<li><a href=\"logout.php\">Logout</a></li>";
}

/*Create Footer
This function ends open HTML tags
*/
function createfooter() {
	echo "</body></html>";
}
?>
