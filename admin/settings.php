<?php
/*Settings page
This page generates all settings options, along with submitting them and updating the database.
This page also allows the adminstrator to update the icons table by either adding new icons or removing old icons
Written by: Jeffrey King
Language: PHP, HTML
*/

session_start();

include('sesvalid.php');
include('adminfuncs.php');
include('pagespawn.php');
include('../inc/dbfuncs.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
//Submit settings before displaying header, so there isn't a need to refresh to see updated header if distribution is toggled
if(isset($_POST['Orders_Open'])) 
	$updatestatus = updateinfo($conn, $userinfo);

$distopen = distopen($conn);
createheader("Settings", $userinfo, $distopen);
if($userinfo['acctype'] <2)
{
	echo "<div class=\"errorbox\">You are not authorized to view this page</div>";
	exit;
}
if(isset($_POST['rmicon']) && $_POST['rmicon'] != "")
	removeicon($conn, $_POST['rmicon'], $userinfo);
if(isset($_POST['iconid']) && isset($_POST['icontext']) && $_POST['iconid'] != "" && $_POST['icontext'] != "")
	addicon($conn, $_POST['iconid'], $_POST['icontext'], $userinfo);
echo $updatestatus;
?>
<p>This page may be used to select settings for the system.
<br>
<form action="settings.php" method="post">
Order Status: <select name="Orders_Open"><?php orderstatus($conn);?></select>
<br><br>MFH Status: <select name="MFH_Open"><?php mfhstatus($conn); ?></select>
<br><br>Distribution Status: <select name="Distribution_Open"><?php diststatus($conn); ?></select>
<br><br>Email Receipts: <select name="Email_Receipts"><?php emailstatus($conn); ?></select>
<br><?php typeoptions($conn); ?>
<br><input type="submit" value="Update Settings">
</form>
<br>
<br>
<div style="border: 1px solid black;">
<div style="margin-left: 3px; width: 500px; float:left;">
<h3>Remove Icons</h3>
<p>Use the below form to remove icons from the icon list.</p>
<!--Remove Icon-->
<form action="settings.php" method="post">
Remove Icon: <select name="rmicon"><?php iops($conn); ?></select>
<br><input type="submit" value="Remove Icon"></form>
</div>
<div style="margin-left:505px;">
<h3>Add Icons</h3>
<p>Use the below form to add icons to the icon list.</p>
<form action="settings.php" method="post">
Icon ID: <input type="number" name="iconid" style="-moz-appearance:textfield; -webkit-appearance: none;" min="1"> &nbsp; Icon Name: <input type="text" name="icontext">
<br><input type="submit" value="Add Icon"></form>
</div>
<br>
</div>
</body>
</html>


<?php
/* Functions */

/*Update Settings
This function loops through all settings and submits an update query to the database with their values as submitted to the form
@param: Database Connection
@param: User account information object
@return: Settings updated message when completed
*/
function updateinfo($conn, $userinfo) {
	/*
	* Error checker - Check if entry for URL is a valid URL
	*/
	if (!filter_var( $_POST['IconURL'], FILTER_VALIDATE_URL ) )
		return "<div class=\"errorbox\">Icon URL is not a valid URL - your URL should be the full URL, including HTTP/HTTPS.<br>Your settings were not saved, please try again</div>";
	$query = "select setname from settings";
	$setlist = $conn->query($query);
	$numrows = $setlist->num_rows;
	for ($i = 0; $i < $numrows; $i++)
	{
		$row = $setlist->fetch_assoc();
		$setval = $_POST[$row['setname']];
		$query = "update settings set setval = \"".$setval."\" where setname = \"".$row['setname']."\"";
		$result = $conn->query($query);
		
	}
	ybk_logger($conn, $userinfo, "Updated System Settings", "Administration");
	return "<div class=\"successbox\">Settings Updated!</div>";
}

/*Display Icons list
This function creates options list of all available icons
(This same function is used in the Orders page)
@param: Database Connection
*/
function iops($conn) {
	$query = "select * from icons";
	$result = $conn->query($query);
	$num_results = $result->num_rows;
	$options = array();
	$options[] = "<option value=''> Select Icon </option>";
	for($i=0; $i < $num_results; $i++)
	{
		$row = $result->fetch_assoc();
		$options[] = "<option value='".$row['icon']."'>".$row['icon']." - ".$row['description']."</option>";
	}
	echo implode("\n", $options);
}

/*Remove Icon
This function removes an icon from the list
@param: Database Connection
@param: IconID
@param: User Account Information Object
*/
function removeicon($conn, $iconid, $userinfo) {
	$query = "delete from icons where icon = \"".$iconid."\"";
	$result = $conn->query($query);
	if (!$result)
	{
		echo "<div class=\"errorbox\">Icon could not be deleted</div>";
	}
	else
	{
		ybk_logger($conn, $userinfo, "Icon ".$iconid." removed from icon list", "Administration");
		echo "<div class=\"successbox\">Icon was deleted from the icons list</div>";
	}
}

/*Add Icon
This function adds an icon to the list
@param: Database Connection
@param: Icon ID
@param: Icon Text
@param: User Account Information Object
*/
function addicon($conn, $iconid, $icontext, $userinfo) {
	$query = "insert into icons values ('$iconid', '$icontext')";
	$result = $conn->query($query);
	if (!$result)
	{
		echo "<div class=\"errorbox\">Icon could not be added - Icon with that Icon ID may already exist.</div>";
	}
	else
	{
		ybk_logger($conn, $userinfo, "Icon ".$iconid.", ".$icontext." was added to the icon list", "Administration");
		echo "<div class=\"successbox\">Icon was added to the icons list</div>";
	}
}

createfooter();
?>
