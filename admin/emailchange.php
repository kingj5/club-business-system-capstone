<?php
/*Change User Email
This page will allow the user to update their email which is on file
Written by: Jeffrey King
Language: PHP
*/
session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('pagespawn.php');
include('../inc/dbfuncs.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Change User Email", $userinfo, $distopen);

//if form has been filled out
if(isset($_POST['oldemail']))
	emailupdate($conn, $userinfo);
//display form
else
{
	changeemail();
}

/*Email Update
This function is run if the email form was filled out, and will validate the change and submit it to the database
@param: Database Connection
@param: User's Account Information Object
*/
function emailupdate($conn, $userinfo)
{
	$oldemail = $_POST['oldemail'];
	if ($oldemail != $userinfo['email'])
	{
		echo "<div class=\"errorbox\">Your Current Email does not match the one on file, please try again.</div>";
		changeemail();
		return;
	}
	$newemail1 = $_POST['newemail1'];
	$newemail2 = $_POST['newemail2'];
	if ($newemail1 != $newemail2)
	{

		echo "<div class=\"errorbox\">Your new emails do not match, please try again.</div>";
		changeemail();
		return;
	}
	else
	{
		$query = "update users set email=\"".$newemail1."\" where uid = \"".$userinfo['uid']."\"";
		$result = $conn->query($query);
		if (!$result)
		{
			echo "<div class=\"errorbox\">Error when updating users database.  Try again later.</div>";
			exit;
		}
		else
		{
			ybk_logger($conn, $userinfo, "User updated own email address", "Account");
			echo "<div class=\"successbox\">Email Updated Successfully!</div>";
			echo "<br><a href=\"account.php\">Click to Return to Account Management</a>";
		}
	}
	return;
}
/*Change Email Function
This function displays the Change Email form to be filled out
*/
function changeemail()
{
	echo "<p>Fill out the below form to change your email</p>
        <form action=\"emailchange.php\" method=\"post\">
        Current Email: <input type=\"email\" name=\"oldemail\" required>
        <br>New Email: <input type=\"email\" name=\"newemail1\" required>
        <br>Confirm New Email: <input type=\"email\" name=\"newemail2\" required>
        <br><input type=\"submit\" value=\"Update Info\">
        </form>";
}

createfooter();
?>
