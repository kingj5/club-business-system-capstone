<?php
/*
Redirect Script
If Session is open, bring to Dashboard
If session does not exist, bring to login
*/
session_start();
if(!isset($_SESSION['uid']) || !isset($_SESSION['sesid']))
	header('Location: login.php');
if(isset($_SESSION['sesid']))
	header('Location: dashboard.php');
?>
