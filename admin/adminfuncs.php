<?php
/* Admin Functions
This script contains miscellaneous functions which have been centralized due to their potential repeated use
Written by Jeffrey King
Language: PHP
*/

/*Order status
This function determines whether orders are open or not, and is used for picking the correct settings in the settings table
@param: Database connection
*/
function orderstatus($conn) {
	$query = "select setval from settings where setname = 'Orders_Open'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$options = array();
	if($row['setval'] == no)
	{
		$options[] = "<option value='no'>Closed</option>";
		$options[] = "<option value='yes'>Open</option>";
	}
	else
	{
		$options[] = "<option value='yes'>Open</option>";
		$options[] = "<option value='no'>Closed</option>";
	}
	echo implode("\n", $options);
}

/*MFH status
This function determines whether MFH orders are open or not, and is used for picking the correct settings in the settings table
@param: Database connection
*/
function mfhstatus($conn) {
	$query = "select setval from settings where setname = 'MFH_Open'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$options = array();
	if($row['setval'] == no)
	{
		$options[] = "<option value='no'>Closed</option>";
		$options[] = "<option value='yes'>Open</option>";
	}
	else
	{
		$options[] = "<option value='yes'>Open</option>";
		$options[] = "<option value='no'>Closed</option>";
	}
	echo implode("\n", $options);
}

/*Distribution status
This function determines whether Distribution is open or not, and is used for picking the correct settings in the settings table
@param: Database connection
*/
function diststatus($conn) {
	$query = "select setval from settings where setname = 'Distribution_Open'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$options = array();
	if($row['setval'] == no)
	{
		$options[] = "<option value='no'>Closed</option>";
		$options[] = "<option value='yes'>Open</option>";
	}
	else
	{
		$options[] = "<option value='yes'>Open</option>";
		$options[] = "<option value='no'>Closed</option>";
	}
	echo implode("\n", $options);
}

/*email status
This function determines whether Email Recipts are enabled or not and picks the appropriate option to be displayed in the Settings table
@param: Database Connection
*/
function emailstatus($conn) {
	$query = "select setval from settings where setname = 'Email_Receipts'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$options = array();
	if($row['setval'] == no)
	{
		$options[] = "<option value='no'>Disabled</option>";
		$options[] = "<option value='yes'>Enabled</option>";
	}
	else
	{
		$options[] = "<option value='yes'>Enabled</option>";
		$options[] = "<option value='no'>Disabled</option>";
	}
	echo implode("\n", $options);
}

/*type options
This function generates the text fields for all other settings on the settings page that do not have a dropdown menu associated with them
@param: Database Connection
*/
function typeoptions($conn) {
	$query="select * from settings where setname != 'Orders_Open' and setname != 'MFH_Open' and setname != 'Distribution_Open' and setname != 'Email_Receipts'";
	$result = $conn->query($query);
	$num_rows = $result->num_rows;
	for($i=0; $i < $num_rows; $i++)
	{
		$row = $result->fetch_assoc();
		echo "<br>".$row['setname'].": <input type='text' name=".$row['setname']." value=\"".$row['setval']."\" required>";
	}
}

/*Distribution Open
This function determines if distribution is open, and is used in generating the header and other areas of the site
@param: Database Connection
@return: setting value
*/
function distopen($conn) {
	$query = "select setval from settings where setname = 'Distribution_Open'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	return $row['setval'];
}

/* Generic Logging Template 
@param: Database Connection
@param: User's account information object
@param: Action taken
@param: Type of action
*/
function ybk_logger($conn, $userinfo, $action, $type) {
	if ($userinfo == "install") 
		$user = "install";
	else
		$user = $userinfo['username'];
	$date = date('Y-m-d H:i:s');
	$query = "insert into logs (user, action, type, date) values ('$user', '$action', '$type', '$date')";
	$result = $conn->query($query);
}

?>
