<?php
/*Search Order Page
This page allows the user to search for order entries that meet their critera
Written by: Jeffrey King
Language: PHP
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("Search Orders", $userinfo, $distopen);
if ($distopen == "no" && $userinfo['acctype'] < 2) {
        echo "<div class=\"errorbox\">Access Denied - Distribution is not open at this time</div>";
        exit;
}

?>

<form method="post" action="searchorder.php">
<br>Search Orders By: <select name="searchby">
<option value="lastname">Last Name</option>
<option value="homeroomteacher">Homeroom Teacher</option>
<option value="grade">Grade</option></select>
&nbsp; Search For: <input type="text" name="searchfor">
&nbsp; <input type="submit" value="Search"></form>
<br><br>
<?php
if (isset($_POST['searchfor']))
{
	$searchby = $_POST['searchby'];
	$searchfor = $_POST['searchfor'];
	if ($searchfor == "") {
		exit;
	}
	$results = getresults($conn, $searchby, $searchfor);
	$num_rows= $results->num_rows;
	echo "There were ".$num_rows." results for your search.<br>";
	echo "You searched by ".$searchby." for ".$searchfor."<br><br>";
	if ($num_rows > 0)
		echoresults($results);
}


/*get search results
This function will take in the search submission and return its results
@param: Database Connection
@param: Search category
@param: Search term
@return: Result set
*/
function getresults($conn, $searchby, $searchfor) {

	$query = "select * from orders where ".$searchby." like \"".$searchfor."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "Error Searching";
		exit;
	}
	return $result;
}

/*echo results
This function will echo out the search results in a table
@param: Result Set
*/
function echoresults($result) {

	echo "<article id=\"ordersearchresults\">";
	for ($i = 0; $i < $result->num_rows; $i++) {
		$row = $result->fetch_assoc();
		echo "<section class=\"resultrow\">";
		echo "<div class=\"resulturl\"><ul><li><a href=\"vieworder.php?ordernum=".$row['ordernum']."\">".$row['firstname']." ".$row['lastname']."</a></li></ul></div>";
		echo "<div class=\"resultinfo\"><ul><li>Grade: ".$row['grade']."</li><li>Homeroom: ".$row['homeroomnumber']." ".$row['homeroomteacher']."</li><li>Number of Books Ordered: ".$row['bookquantity']."</li></ul></div>";
		echo "<div class=\"resultstatus\"><ul><li>Order Status: ";
		if($row['orderstatus'] == "")
			echo "Not Distributed</li></ul>";
		else {
			echo "Distributed</li>";
			echo "<li>Distributed by: ".$row['orderupdate']." on ".$row['orderdateupdate'];
		}
		echo "</li></ul></div>";
		echo "</section>";
	}
	echo "</article>";
}

createfooter();
?>
