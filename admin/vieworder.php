<?php
/*View Order Page
This page is used for viewing information about orders, editing orders, deleting orders, and distributing orders
Written by Jeffrey King
Language: PHP, HTML, JavaScript
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("View Orders", $userinfo, $distopen);
if ($distopen == "no" && $userinfo['acctype'] < 2) {
	echo "<div class=\"errorbox\">Access Denied - Distribution is not open at this time</div>";
	exit;
}
?>
<form method="post" action="searchorder.php">
<br>Search Orders By: <select name="searchby">
<option value="lastname">Last Name</option>
<option value="homeroomteacher">Homeroom Teacher</option>
<option value="grade">Grade</option></select>
&nbsp; Search For: <input type="text" name="searchfor">
&nbsp; <input type="submit" value="Search"></form>
<br><br>
<?php
if (!isset($_GET['ordernum'])) {
	exit;
}

//Delete order
if (isset($_GET['action']) && $_GET['action'] == "delete") {
	$query = "delete from orders where ordernum = \"".$_GET['ordernum']."\"";
	$result = $conn->query($query);
	if (!$result) {
		echo "<div class=\"errorbox\">Error deleting entry</div>";
	}
	else {
		ybk_logger($conn, $userinfo, "Deleted order ".$_GET['ordernum'], "Administration");
		echo "<div class=\"successbox\">Order ".$_GET['ordernum']." deleted successfully</div>";
		exit;
	}
}

//Edit order
if (isset($_GET['action']) && $_GET['action'] == "edit" && $distopen == "no") {
	$query = "select * from orders where ordernum = \"".$_GET['ordernum']."\"";
	$result = $conn->query($query);
	if (!$result) {
        	echo "Error with query";
        	exit;
	}

	$row = $result->fetch_assoc();
	echo "<h3>Edit Order Information for Order #".$row['ordernum'].", ".$row['firstname']." ".$row['lastname']."</h3>

	<form method=\"post\" action=\"vieworder.php?ordernum=".$_GET['ordernum']."\">
	Grade: <input type=\"text\" name=\"grade\" value=\"".$row['grade']."\" required>
	<br>Homeroom Teacher: <input type=\"text\" name=\"homeroomteacher\" value=\"".$row['homeroomteacher']."\" required>
	<br>Homeroom Number: <input type=\"number\" name=\"homeroomnumber\" value=\"".$row['homeroomnumber']."\" style=\"-moz-appearance: textfield; -webkit-appearance: none;\" required>
	<br>Email Address: <input type=\"email\" name=\"email\" value=\"".$row['emailadd']."\" required>
	<br>Phone Number: <input type=\"text\" name=\"phone\" value=\"".$row['phone']."\" required>
	<br>Book Quantity: <input type=\"number\" name=\"bookquantity\" value=\"".$row['bookquantity']."\" min=\"1\" style=\"-moz-appearance: textfield; -webkit-appearance: none;\" required>
	<br>Personalization Name: <input type=\"text\" name=\"personalname\" value=\"".$row['personalname']."\">
	<br>Personalization Icon 1: <select name=\"person_icon1\">";
        iops2($conn, $row['person_icon1']);
        echo "</select>
	<br>Personalization Icon 2: <select name=\"person_icon2\">";
        iops2($conn, $row['person_icon2']);
        echo "</select>
	<br>Personalization Icon 3: <select name=\"person_icon3\">";
        iops2($conn, $row['person_icon3']);
        echo "</select>
	<br>Personalization Icon 4: <select name=\"person_icon4\">";
        iops2($conn, $row['person_icon4']);
        echo "</select>
	<br><input type=\"submit\" value=\"Update\">
	</form>";
	exit;
}

//Update order with edited information
if (isset($_POST['grade'])) {
	//Shorter Variables
	$grade = $_POST['grade'];
	$homeroomteacher = $_POST['homeroomteacher'];
	$homeroomnumber = $_POST['homeroomnumber'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$bookquantity = $_POST['bookquantity'];
	$personalname = $_POST['personalname'];
	$person_icon1 = $_POST['person_icon1'];
	$person_icon2 = $_POST['person_icon2'];
	$person_icon3 = $_POST['person_icon3'];
	$person_icon4 = $_POST['person_icon4'];

	$query = "select setval from settings where setname = 'Book_Price'";
	$result = $conn->query($query);
	$row=$result->fetch_assoc();
	$bookprice = $row['setval'];

	$query = "select setval from settings where setname = 'Icon_Price'";
	$result = $conn->query($query);
	$row=$result->fetch_assoc();
	$iconprice = $row['setval'];

	$price = $bookprice * $bookquantity;
	if($person_icon1 != "")
        	$price += $iconprice;
	if($person_icon2 != "")
        	$price += $iconprice;
	if($person_icon3 != "")
        	$price += $iconprice;
	if($person_icon4 != "")
        	$price += $iconprice;

	$query="update orders set grade=\"".$grade."\", homeroomteacher=\"".$homeroomteacher."\", homeroomnumber=\"".$homeroomnumber."\", emailadd=\"".$email."\", phone=\"".$phone."\", bookquantity=\"".$bookquantity."\", personalname=\"".$personalname."\", person_icon1=\"".$person_icon1."\", person_icon2=\"".$person_icon2."\", person_icon3=\"".$person_icon3."\", person_icon4=\"".$person_icon4."\", price=\"".$price."\" where ordernum=\"".$_GET['ordernum']."\"";
	$result=$conn->query($query);
	if(!$result) {
		echo "<div class=\"errorbox\">Error updating information</div>";
	}
	else {
		ybk_logger($conn, $userinfo, "Updated order ".$_GET['ordernum'], "Administration");
		echo "<div class=\"successbox\">Order Information Updated Successfully.</div>";
	}
}

//Distribute Book
if (isset($_POST['distribute'])) {
	$status = $_POST['distribute'];
	$date = date('Y-m-d H:i:s');
	if($status == "distributed") {
		$query = "update orders set orderstatus = \"".$status."\", orderupdate = \"".$userinfo['name']."\", orderdateupdate = \"".$date."\" where ordernum = \"".$_GET['ordernum']."\"";
		$result = $conn->query($query);
		if (!$result) {
			echo "<div class=\"errorbox\">Error updating database</div>";
		}
		else {
			ybk_logger($conn, $userinfo, "Distributed book for order ".$_GET['ordernum'], "Distribution");
			echo "<div class=\"successbox\">Book Distribution Status Updated</div><br>";
		}
	}
	else if($status == "undo") {
		$query = "update orders set orderstatus = '', orderupdate = \"".$userinfo['name']."\", orderdateupdate = \"".$date."\" where ordernum = \"".$_GET['ordernum']."\"";
		$result = $conn->query($query);
                if (!$result) {
                        echo "<div class=\"errorbox\">Error updating database</div>";
                }
                else {
			ybk_logger($conn, $userinfo, "Undid distribution for order ".$_GET['ordernum'], "Administration");
                        echo "<div class=\"successbox\">Book Distribution Status Updated</div><br>";
                }
        }
}

//Display Order Information
$query = "select * from orders where ordernum = \"".$_GET['ordernum']."\"";
$result = $conn->query($query);
if (!$result) {
	echo "Error with query";
	exit;
}

$row = $result->fetch_assoc();
echo "<h3>Order Information for Order #".$row['ordernum'].", ".$row['firstname']." ".$row['lastname']."</h3>";
echo "<div id=\"orderbox\">
<div id=\"orderleftpanel\"><div class=\"paneltitle\">Order Information</div>
<ul><li>Name: ".$row['firstname']." ".$row['lastname']."</li>
<li>Grade: ".$row['grade']."</li>
<li>Homeroom Teacher: ".$row['homeroomteacher']."</li>
<li>Homeroom Number: ".$row['homeroomnumber']."</li>
<li>Email Address: ".$row['emailadd']."</li>
<li>Phone Number: ".$row['phone']."</li>
<li>Book Quantity: ".$row['bookquantity']."</li>
<li>Date Ordered: ".$row['date']."</li></ul>
</div><div id=\"orderrightpanel\"><div class=\"paneltitle\">Order Status</div>
<ul><li>Distribution Status: ";
if ($row['orderstatus'] == "") {
	echo "Book not distributed</li>";
}
else if ($row['orderstatus'] == "distributed") {
	echo "Book distributed <ul><li>Distributed by ".$row['orderupdate']."</li><li>Distributed on ".$row['orderdateupdate']."</li></ul></li>";
}
else {
	echo "Could not determine distribution status";
}
echo "</ul><br>
<hr style=\"width: 70%;\"><br>
<div class=\"paneltitle\">Options</div>
<ul><li><form method=\"post\" action=\"vieworder.php?ordernum=".$_GET['ordernum']."\">Distribute Book: ";
if ($row['orderstatus'] == "distributed" && $userinfo['acctype'] >= 2) {
echo "<select name=\"distribute\"><option value=\"-\">-Pick Option-</option><option value=\"undo\">Undo Distribute</option></select><input type=\"submit\" value=\"Submit\"></form></li>";
}
else if ($row['orderstatus'] != "distributed") {
echo "<select name=\"distribute\"><option value=\"-\">-Pick Option-</option><option value=\"distributed\">Distribute Book</option></select><input type=\"submit\" value=\"Submit\"></form></li>";
}
else {
echo "<select name=\"distribute\" disabled><option value=\"-\">-Pick Option-</option><option value=\"distributed\">Distribute Book</option></select><input type=\"submit\" value=\"Submit\" disabled></form></li>";
}
if ($userinfo['acctype'] >= 2) {
	if($distopen == "no") {
		echo "<li><a href=\"vieworder.php?ordernum=".$_GET['ordernum']."&action=edit\">[Admin] Edit Order</a></li>
		<li><a href=\"vieworder.php?ordernum=".$_GET['ordernum']."&action=delete\" onclick='return confirmdel();'>[Admin] Delete Order</a></li>";
	}
	else {
		echo "<li><a href=\"vieworder.php?ordernum=".$_GET['ordernum']."&action=delete\" onclick='return confirmdel();'>[Admin] Delete Order</a></li>";
	}
}
echo "
</ul></div>
</div>";

/*Icon Options
Slightly modified version of the previous icon options scripts, this one will select by default the icon which is currently selected in the database and generate the entire list
@param: Database Connection
@param: IconID of current Icon
*/
function iops2($conn, $iconid) {
        $query = "select * from icons";
        $result = $conn->query($query);
        $num_results = $result->num_rows;
        $options = array();
        $options[] = "<option value=''> Select Icon </option>";
        for($i=0; $i < $num_results; $i++)
        {
                $row = $result->fetch_assoc();
		if($row['icon'] == $iconid) {
	                $options[] = "<option value='".$row['icon']."' selected=\"selected\">".$row['icon']." - ".$row['description']."</option>";
		}
		else
                	$options[] = "<option value='".$row['icon']."'>".$row['icon']." - ".$row['description']."</option>";
        }
        echo implode("\n", $options);
}

createfooter();
?>

<script type="text/javascript">
//Confirm Deletion
//Displays confirmation message to check if user actually wants to delete order
function confirmdel() {
	var deleteit = confirm("Are you sure you want to delete this order?  This action cannot be undone.");
	return deleteit;
}
</script>

