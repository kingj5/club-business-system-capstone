<?php
/*View Logs 
This page allows an administrator to view the system logs
Written by: Jeffrey King
Language: PHP, HTML
*/

session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("View System Logs", $userinfo, $distopen);
if($userinfo['acctype'] < 2) {
	echo "<div class=\"errorbox\">Access denied - you do not have permission to view this page.</div>";
	exit;
} 

if (isset($_POST['logtype']) && $_POST['logtype'] != "all") {
	$query = "select * from logs where type = \"".$_POST['logtype']."\" order by lid desc";
	$notype = false;
}
else {
	$query = "select * from logs order by lid desc";
	$notype = true;
}
?>
<div id="mkanntitle"><form method="post" action="viewlogs.php">
Select Category of logs to view: 
<select name="logtype">
<option value="all">All</option>
<option value="Account">Account</option>
<option value="Administration">Administration</option>
<option value="Announcements">Announcements</option>
<option value="Distribution">Distribution</option>
<option value="Installation">Installation</option>
<option value="Reports">Reports</option></select>
&nbsp; &nbsp; <input type="submit" value="Generate Logs"></form></div>
<?php
$result = $conn->query($query);
if (!$result) {
	echo "<div class=\"errorbox\">Error fetching results</div>";
	exit;
}

$numrows = $result->num_rows;
if ($numrows == 0) {
	echo "No results to display";
	exit;
}

echo "<h2 id=\"h2center\">System Logs";
if (!$notype)
	echo " by type: ".$_POST['logtype']."</h2>";
else
	echo "</h2>";

echo "<div class=\"ordertable\"><div class=\"orderrow\">
<div class=\"tablecell-b\">User</div>
<div class=\"tablecell-b\">Action</div>
<div class=\"tablecell-b\">Date</div>";
if ($notype)
	echo "<div class=\"tablecell-b\">User</div>";
echo "</div>";
for ($i = 0; $i < $numrows; $i++) {
	$row = $result->fetch_assoc();
	echo "<div class=\"orderrow\">
	<div class=\"tablecell\">".$row['user']."</div>
	<div class=\"tablecell\">".$row['action']."</div>
	<div class=\"tablecell\">".$row['date']."</div>";
	if ($notype)
		echo "<div class=\"tablecell\">".$row['type']."</div>";
	echo "</div>";
	}
echo "</div>";

createfooter();
?>
