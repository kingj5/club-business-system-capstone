<?php
/*Validate Login
This page checks if the login was valid.  If the login was valid, a session will be created and the user directed to the dashboard.
If the login was valid but the user has a temporary password assigned, the user will be forced to create a new password.
If the login was not valid, the user will be presented once again with the Login screen
Written by Jeffrey King
Language: PHP, HTML
*/

session_start();
error_reporting(0);
?>

<html>
<head>
<link rel="stylesheet" href="admin.css">
<link rel="stylesheet" href="../inc/global.css">
<title>Yearbook Business System</title>
</head>
<body>
<?php
if(isset($_SESSION['sesid']))
{
	header('Location: dashboard.php');
	exit;
}

$user = $_POST['username'];
$pass = sha1($_POST['password']);
$date = date('Y-m-d H:i:s');
if (isset($_POST['newpass1']))
	$newpass1 = sha1($_POST['newpass1']);
if (isset($_POST['newpass2']))
	$newpass2 = sha1($_POST['newpass2']);

include('../inc/dbfuncs.php');
$conn = dbconn();
if (isset($_POST['newpass1']) && isset($_POST['newpass2']))
	setpass($newpass1, $newpass2, $conn);

$query = "select * from users where username = \"".$user."\" and pass = \"".$pass."\"";
$result = $conn->query($query);
$count = $result->num_rows;
if ($count == 1)
{
	//echo "Login Successful.  Redirecting...";
	$row=$result->fetch_assoc();
	$uid = $row['uid'];
	$_SESSION['uid'] = $uid;
	if ($row['accstatus'] == 1)
	{
		temppw();
	}
	else if ($row['accstatus'] == 2) {
		$deleted = true;
		unset($_SESSION['uid']);
		session_destroy();
	}
	else
	{
		$_SESSION['sesid'] = sha1($date.$user);
		$query="update users set lastlogin = \"".$date."\", sessionid = \"".$_SESSION['sesid']."\" where uid = \"".$uid."\"";
		$result = $conn->query($query);
		header('Location: dashboard.php');
		exit;
	}
}
else
	session_destroy(); //Not valid login, no session.

/*temp password
This function generates the temporary password form, allowing the user to create a new password
*/
function temppw() {
	echo "<html>
	<head>
	<link rel=\"stylesheet\" href=\"../inc/global.css\">
	<title>Yearbook Business System - Set New Password</title>
	</head>";
	echo "Your Password was assigned by an administrator to be a temporary password.  Please set a new password";
	echo "<form action=\"validate.php\" method=\"post\">";
	echo "<br>New Password: <input type=\"password\" name=\"newpass1\" required>";
        echo "<br>Confirm New password: <input type=\"password\" name=\"newpass2\" required>";
        echo "<br><input type=\"submit\" value=\"Update Info\">";
        echo "</form>";
	exit;
}

/*set password
This function will perform appropriate validation checks and set the new password if the user has logged in with a temporary password
@param: new password (twice)
@param: Database Connection
*/
function setpass($newpass1, $newpass2, $conn) {
	if ($newpass1 != $newpass2)
        {
                echo "Your new passwords do not match, please try again.<br><br>";
                temppw();
                return;
        }
        else
        {
                $query = "select * from users where uid = \"".$_SESSION['uid']."\"";
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		if ($newpass1 == $row['pass'])
		{
			echo "You can't set the same password as your temporary password!  Please try again<br><br>";
			temppw();
			return;
		}
		$query = "update users set pass=\"".$newpass1."\", accstatus=\"0\" where uid = \"".$_SESSION['uid']."\"";
                $result = $conn->query($query);
                if (!$result)
                {
                        echo "Something failed";
                        exit;
                }
                else
                {
                        echo "Password Updated Successfully";
                        echo "<br><a href=\"login.php\">Click to login.</a>";
                	exit;
		}
        }
        return;
}

/*Display Login Page*/

?>
<html>
<head>
<title>Yearbook Business System - Admin Login</title>
<link rel="stylesheet" href="../inc/global.css">
</head>
<body>
<h1>Yearbook Business System - Admin Login</h1>
<br><?php
if ((isset($_POST['username']) || isset($_POST['password'])) && !isset($deleted))
{
	echo "<div class=\"errorbox\">Log-in Error: Invalid Username or Password</div>";
}
if (empty($_POST['username']) || empty($_POST['password']))
{
	echo "<div class=\"errorbox\">Log-in Error: One of the fields was left blank.</div>";
}

if (isset($deleted) && deleted == true)
{
	echo "<div class=\"errorbox\">Your account has been closed by the system administrator.</div>";
}
?>
<form action="validate.php" method="post">
Username: <input type="text" name="username" required>
<br>Password: <input type="password" name="password" required>
<br><input type="submit" value="Submit"></form>
</body>
</html>
