<?php
/*Message From Home Viewer
This page will display all Message From Home entries in the order that they will be listed in the book, and will divide them up 10 per page
Written by Jeffrey King
Language: PHP
*/
session_start();
error_reporting(0);

include('sesvalid.php');
include('adminfuncs.php');
include('../inc/dbfuncs.php');
include('pagespawn.php');
$conn = dbconn();
$userinfo = sesvalid($conn);
$distopen = distopen($conn);
createheader("View Messages From Home Submissions", $userinfo, $distopen);

$query = "select firstname, lastname, message, picture from mfh order by lastname, firstname";
$result = $conn->query($query);
if (!$result) {
	echo "<div class=\"errorbox\">Error generating MFH information</div>";
	exit;
}

$numrows = $result->num_rows;
if ($numrows == 0) {
	echo "No results to display";
	exit;
}
ybk_logger($conn, $userinfo, "User viewed MFH Submissions", "Reports");
$count = 1;
echo "<div id=\"mfhpageselect\">Jump to page: <br>";
for ($i = 1; $i <= ceil($numrows/10); $i++) {
	echo "<a href=\"#mfhpg".$i."\">".$i."</a> ";
	if ($i != ceil($numrows/10))
		echo "- ";
}
echo "</div>";
for ($i = 0; $i < $numrows; $i++) {
	if ($i % 10 == 0) {
		echo "<h3 id=\"mfhpg".$count."\">Page ".$count."</h3> 
		<div class=\"mfhtable\">
		<div class=\"mfhrow\">
		<div class=\"mfhname\">Student Name</div>
		<div class=\"mfhlink\">Image Link</div>
		<div class=\"mfhmess\">Message</div></div>";
	}

	$row = $result->fetch_assoc();
	echo "<div class=\"mfhrow\">
	<div class=\"mfhname\">".$row['firstname']." ".$row['lastname']."</div>
	<div class=\"mfhlink\"><a href=\"../uploads/".$row['picture']."\">Click to view image</a>
	<br>
	<a href=\"../uploads/".$row['picture']."\" download=\"../uploads/".$row['picture']."\">Click to download image</a>
	</div>
	<div class=\"mfhmess\">".nl2br($row['message'])."</div></div>";
	
	if ($i % 10 == 9 || $i-1==$numrows) {
		echo "</div><br>
		<a href=\"#mfhpageselect\">Back to the top</a>
		<br>";
		$count++;
	}
}

createfooter();
?>
