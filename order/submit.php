<!DOCTYPE html>
<html>
<head>
<title>Yearbook Business System - Order Submission</title>
<link rel="stylesheet" href="../inc/global.css">
</head>
<body>
<h1>Yearbook Business System - Order Submission</h1>

<?php
/* Submit Order Page
Written by Jeffrey King
Language: PHP, HTML
*/

//Disable PHP Error Reporting
error_reporting(0);

//Get shorter variables
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$homeroomteacher = $_POST['homeroomteacher'];
$homeroomnumber = $_POST['homeroomnumber'];
$grade = $_POST['grade'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$quantity = $_POST['bookquantity'];
$personalname = $_POST['personalname'];
$icon1 = $_POST['icon1'];
$icon2 = $_POST['icon2'];
$icon3 = $_POST['icon3'];
$icon4 = $_POST['icon4'];
$cardnumb = $_POST['cardnumb'];
$cardtype = $_POST['cardtype'];
$cardname = $_POST['cardname'];

//Establish Database connection
include ('../inc/dbfuncs.php');
$conn = dbconn();

if (!isset($_POST['firstname'])) {
	echo "<div class=\"errorbox\">No order information was received.  Please re-visit the <a href=\"order.php\">Order page</a> and try to submit your order again.</div>";
	exit;
}

//Grab prices of books and icons
$query = "select setval from settings where setname = 'Book_Price'";
$result = $conn->query($query);
$row=$result->fetch_assoc();
$bookprice = $row['setval'];

$query = "select setval from settings where setname = 'Icon_Price'";
$result = $conn->query($query);
$row=$result->fetch_assoc();
$iconprice = $row['setval'];

//Calculate order price
$price = $bookprice * $quantity;
if($icon1 != "")
	$price += $iconprice;
if($icon2 != "")
	$price += $iconprice;
if($icon3 != "")
	$price += $iconprice;
if($icon4 != "")
	$price += $iconprice;

$date = date('Y-m-d H:i:s');

//Insert data into database
$query = "insert into orders (firstname, lastname, homeroomteacher, homeroomnumber, grade, emailadd, phone, bookquantity, personalname, person_icon1, person_icon2, person_icon3, person_icon4, price, date, cardnum, cardtype, cardname) values ('$firstname', '$lastname', '$homeroomteacher', '$homeroomnumber', '$grade', '$email', '$phone', '$quantity', '$personalname', '$icon1', '$icon2', '$icon3', '$icon4', '$price', '$date', '$cardnumb', '$cardtype', '$cardname')";
$result = $conn->query($query);

if (!$result)
{
	echo "<div class=\"errorbox\">Order could not be submitted at this time due to a database error.  Please try again later.</div>";
	exit;
}

//Grab order number from database, used in generating receipt
$query = "select ordernum from orders where firstname = \"".$firstname."\" and lastname = \"".$lastname."\" and homeroomteacher = \"".$homeroomteacher."\" and emailadd = \"".$email."\"";
$result = $conn->query($query);

if (!$result)
{
	echo "Error obtaining order number";
	exit;
}

$num_results = $result->num_rows;
for ($i = 0; $i < $num_results; $i++) {
	$row = $result->fetch_assoc();
}
//Generate Receipt
?>
Your order has been successfully submitted!  Below is your receipt.  Please print this page for your records.<br><br>

<?php

//msg - save for use in email receipts
$msg = "Your yearbook order has been successfully submitted.  Below is a copy of your receipt.  Please save this email for your records.";

echo "Order Number: ".$row['ordernum'];
echo "<br>Time of Order: ".$date;
echo "<br>Student Name: ".$firstname." ".$lastname;
echo "<br>Homeroom Info (teacher/number): ".$homeroomteacher.", Room ".$homeroomnumber;
echo "<br>Grade: ".$grade;
echo "<br>Email: ".$email;
echo "<br>Phone: ".$phone;
echo "<br>Book Quantity: ".$quantity;

$msg .= "<br><br>Order Number: ".$row['ordernum']."<br>
Time of Order: ".$date."<br>
Student Name: ".$firstname." ".$lastname."<br>
Homeroom Info (teacher/number): ".$homeroomteacher.", Room ".$homeroomnumber."<br>
Grade: ".$grade."<br>
Email: ".$email."<br>
Phone: ".$phone."<br>
Book Quantity: ".$quantity."<br>";

if ($grade == 12 && $personalname != "")
{
	echo "<br>Personalization Name: ".$personalname;
	$msg .= "Personalization Name: ".$personalname."<br>";
}
if ($grade == 12 && $icon1 != "")
{
	$query="select description from icons where icon = \"".$icon1."\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo "<br>Icon 1: ".$icon1." - ".$row['description'];
	$msg .= "Icon 1: ".$icon1." - ".$row['description']."<br>";
}
if ($grade == 12 && $icon2 != "")
{
	$query="select description from icons where icon = \"".$icon2."\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo "<br>Icon 2: ".$icon2." - ".$row['description'];
	$msg .= "Icon 2: ".$icon2." - ".$row['description']."<br>";
}
if ($grade == 12 && $icon3 != "")
{
	$query="select description from icons where icon = \"".$icon3."\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo "<br>Icon 3: ".$icon3." - ".$row['description'];
	$msg .= "Icon 3: ".$icon3." - ".$row['description']."<br>";
}
if ($grade == 12 && $icon4 != "")
{
	$query="select description from icons where icon = \"".$icon4."\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo "<br>Icon 4: ".$icon4." - ".$row['description'];
	$msg .= "Icon 4: ".$icon4." - ".$row['description']."<br>";
}
echo "<br>Total Paid: $".$price;

echo "<br><br>Thank you for your order!  Check back in May for information regarding Distribution.";
$msg .= "Total Paid: $".$price."<br><br> Thank you for your order!  Check back in May for information regarding Distribution.";

//Send email receipt
sendmail($conn, $msg);

/*sendmail
This function will check if email receipts are turned on, and if they are will generate and send out an email receipt to the orderer.
@param: Database Connection
@param: Message to send
*/
function sendmail($conn, $msg) {
	$query = "select setval from settings where setname = 'Email_Receipts'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	if ($row['setval'] == "yes") {
		/*Import PHPMailer class
		PHPMailer code is used under the GNU LGPL 2.1 License
		PHPMailer code source: https://github.com/PHPMailer/PHPMailer
		*/
		require "../inc/class.phpmailer.php";
		$mail = new PHPMailer;
		$mail->FromName = "Yearbook Business System";
		$mail->addAddress($_POST['email']);
		$mail->isHTML(true);
		$mail->Subject= "Your Yearbook Order Receipt";
		$mail->Body = $msg;
		$mail->send();
	}
}
?>
