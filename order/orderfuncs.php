<?php
/* Order Specific Functions
Written by Jeffrey King
Language: PHP
*/

error_reporting(0);
include ('../inc/dbfuncs.php');

/* iops
This function generates the icon listing for the 4 icon spots on the Orders page
@param: database connection
*/
function iops($conn)
{
	$query = "select * from icons";
	$result = $conn->query($query);
	$num_results = $result->num_rows;
	$options = array();
	$options[] = "<option value=''> Select Icon </option>";
	for($i=0; $i < $num_results; $i++)
	{
		$row = $result->fetch_assoc();
		$options[] = "<option value='".$row['icon']."'>".$row['icon']." - ".$row['description']."</option>";
	}
	echo implode("\n", $options);
}

/* checkopen
This function checks to determine if orders are open.  If orders are not open, the page will not load and will present an error screen instead.
@param: database connection
*/
function checkopen($conn) {
	$query = "select setval from settings where setname = 'Orders_Open'";
	$result = $conn->query($query);
	if(!$result) {
        	echo "<p>Unable to determine if orders are open; please contact administrator</p>";
        	exit;
	}
	$row = $result->fetch_assoc();
	if($row['setval'] == 'no')
	{
       		echo "<p>Orders are not currently open; try again some other time</p>";
       	 	$open = false;
	}
	else
		$open = true;
	return $open;
}

/* getyear
This function gets the year set in the settings table to display on the form
@param: Database connection
*/
function getyear($conn) {
	$query = "select setval from settings where setname = 'Year'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo $row['setval'];
}

/* getprice
This function gets the current book price set in the settings table for use in calculating the total price
@param: Database connection
*/
function getprice($conn) {
	$query = "select setval from settings where setname = 'Book_Price'";
	$result = $conn->query($query);
	$row=$result->fetch_assoc();
	echo $row['setval'];
}

/* getprice
This function gets the current book price set in the settings table for use in calculating the total price
@param: Database connection
*/
function geticonprice($conn) {
	$query = "select setval from settings where setname = 'Icon_Price'";
	$result = $conn->query($query);
	$row=$result->fetch_assoc();
	echo $row['setval'];
}

/* linkicons
This function gets the link provided in the settings table to link to a page which contains the list of icons and their relevant pictures
@param: Database connection
*/
function linkicons($conn) {
	$query = "select setval from settings where setname = 'IconURL'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	echo $row['setval'];
}


?>


