/* JavaScript order functions
Written by Jeffrey King
Language: JavaScript
*/

/*checkGrade
This function checks to see which grade was selected.  If the grade is 12, the personalization fields will be enabled.  Else, they will be disabled and re-set to their initial values.
*/
function checkGrade()
{
	var grade = document.getElementById('gradeselect').value
	if (grade==12)
	{
		document.getElementById('personalname').disabled=false;
		document.getElementById('transname').disabled=false;
		document.getElementById('icon1').disabled=false;
		document.getElementById('icon2').disabled=false;
		document.getElementById('icon3').disabled=false;
		document.getElementById('icon4').disabled=false;
	}
	else
	{
		document.getElementById('personalname').disabled=true;
		document.getElementById('personalname').value="";
		document.getElementById('transname').disabled=true;
		document.getElementById('icon1').disabled=true;
		document.getElementById('icon1').value="";
		document.getElementById('icon2').disabled=true;
		document.getElementById('icon2').value="";
		document.getElementById('icon3').disabled=true;
		document.getElementById('icon3').value="";
		document.getElementById('icon4').disabled=true;
		document.getElementById('icon4').value="";

	}		
}

/*charcount
This function counts the number of characters that have been entered into the personalization box.  The function updates the counter live upon every keypress
*/
function charcount() {
	var maxchar = 29; //Using the Shen/Jostens model, may make this a DB field later.
	var curcount = document.getElementById('personalname').value.length;
	var countleft = maxchar-curcount;
	document.getElementById('personalcount').innerHTML=countleft + " characters remaining.";
}

/*transfername
This function takes the first and last name provided earlier in the order form and will transfer them to the personalization box.  If the name does not fit in the box, an error will be displayed.
*/
function transfername() {
	var fnamecount = document.getElementById('firstname').value.length;
	var lnamecount = document.getElementById('lastname').value.length;
	if (fnamecount == 0 || lnamecount == 0) {
//		do nothing
	}
	else if ((fnamecount + lnamecount) > 29) {
		alert("Name won't fit in personalization box");
	}

	else {
		document.getElementById('personalname').value=document.getElementById('firstname').value + " " +  document.getElementById('lastname').value;
		charcount();
	}
}
