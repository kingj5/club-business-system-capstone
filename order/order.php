<?php
/* Order form
Written by Jeffrey King
Written in: PHP, JavaScript, HTML
*/

error_reporting(0);
//Establish connection to the database
include ('../inc/config.php');
include ('orderfuncs.php');
$conn = dbconn();

?>

<!DOCTYPE html>
<html>
<head>
<title>Yearbook Business System - Order Form</title>
<script type="text/javascript" src="order.js"></script>
<link rel="stylesheet" href="../inc/global.css">
</head>
<body>
<h1>Yearbook Business System - Order Form</h1>
<?php
$open = checkopen($conn);
if (!$open)
	exit;
?>

<p>If you're looking to order a <?php getyear($conn); ?> yearbook, please fill out the below fields</p>
<form action="submit.php" method="post">
<br>First name: <input type="text" id="firstname"  name="firstname" required>
<br>Last name: <input type="text" id="lastname"  name="lastname" required>
<br>Homeroom Teacher: <input type="text" name="homeroomteacher" required>
<br>Homeroom Number: <input type="number" name="homeroomnumber" style="-moz-appearance:textfield; -webkit-appearance: none;" min="0" required>
<br>Grade: <select name="grade" id="gradeselect" onchange="checkGrade(); updateprice(); charcount();">
<option value=""> Select Grade</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option></select>
<br>Email address: <input type="email" name="email" required>
<br>Phone Number: <input type="text" name="phone" required>
<br>Book Quantity: <input type="number" name="bookquantity" id="quantity" onkeyup = "checkprice();" onchange = "checkprice();" style="-moz-appearance:textfield; -webkit-appearance: none;" min="1" required>
<div id="errquant"></div>
<br>
<br><div style="float:left;">Current Order Price: &nbsp </div> <div id="price">$0</div>
<br><script type="text/javascript">
//checkprice
//this function checks to make sure the quantity is valid (greater than 1), and then calls update price to update the price which is displayed on the form
function checkprice() {
var quantity = document.getElementById('quantity').value;
if (isNaN(quantity)) {
	document.getElementById('errquant').innerHTML = "Your quantity must be a number";
	document.getElementById('price').innerHTML = "$0";
	document.getElementById('price2').innerHTML = "$0";
}
else if (quantity < 1) {
	document.getElementById('errquant').innerHTML = "Your quantity must be a positive number";
        document.getElementById('price').innerHTML = "$0";
        document.getElementById('price2').innerHTML = "$0";
}
else {
	document.getElementById('errquant').innerHTML = "";
	updateprice();
}
}

//update price
//This function grabs the price of both icons and books and uses them to calculate the order total price as the order is updated.
function updateprice() {
var price = <?php getprice($conn); ?>;
var iconprice = <?php geticonprice($conn); ?>;
var quantity = document.getElementById('quantity').value;
var orderprice = price * quantity;
if(document.getElementById('icon1').value != '') {
	orderprice += iconprice;
}
if(document.getElementById('icon2').value != '') {
	orderprice += iconprice;
}
if(document.getElementById('icon3').value != '') {
	orderprice += iconprice;
}
if(document.getElementById('icon4').value != '') {
	orderprice += iconprice;
}
document.getElementById('price').innerHTML = "$" + orderprice;
document.getElementById('price2').innerHTML = "$" + orderprice;
}
</script>
<hr>
<h3>Seniors Only: Personalization Info</h3>
Seniors may have their name added to the front cover of the book, free of charge.  Personalizations have a 29 character maximum.
<br><br>Personalization Name: <input type="text" name="personalname" id="personalname" oninput="charcount();" maxlength="29" disabled> &nbsp; <button type="button" id="transname" onclick="transfername();" disabled>Transfer Name</button>
<br><div style="float:left;">You have&nbsp</div><div id="personalcount">29 characters remaining.</div>
<br><br>Up to 4 icons may be added to the front cover of the book.  Each icon is an additional $3 to the total order cost.
<br>To view the icons that can be added to the book, <a href="
<?php linkicons($conn); ?>
">click here</a>.
<br><br>Icon 1: <select name="icon1" id="icon1" onchange = "updateprice();" disabled><?php iops($conn);?></select> &nbsp &nbsp
Icon 2: <select name="icon2" id="icon2" onchange = "updateprice();" disabled><?php iops($conn);?></select> &nbsp &nbsp
Icon 3: <select name="icon3" id="icon3" onchange = "updateprice();" disabled ><?php iops($conn);?></select> &nbsp &nbsp
Icon 4: <select name="icon4" id="icon4" onchange = "updateprice();" disabled><?php iops($conn);?></select>
<br><br><div style="float:left;">Current Order Price: &nbsp </div> <div id="price2">$0</div>
<br><hr>
<h3>Payment Info</h3>
<i>This section has been disabled for the purposes of this project.  In a real system, these would be activated.</i>
<br>Credit Card Number: <input type="text" name="cardnumb" disabled>
<br>Type of Card: <select name="cardtype" disabled><option value="">Select Card Type<option value="mc">MasterCard</option><option value="visa">Visa</option><option value="discover">Discover</option><option value="amex">American Express</option></select>
<br>Name on Card: <input type="text" name="cardname" disabled>
<br><br>
<input type="submit" value="Submit Order">
</form>
<?php $conn->close(); ?>
</body>
</html>
